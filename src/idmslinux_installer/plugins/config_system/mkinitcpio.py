# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Configure system mkinitcpio."""

from typing import Callable, Dict, List, Optional

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugin import Plugin
from idmslinux_installer.util.sysmkinitcpio import SysMkinitcpio


# Ignore warning that we have not overridden all base class methods.
# pylama:ignore=W:select=W023
class ConfigSystemMkinitcpio(Plugin):
    """Configure system mkinitcpio."""

    def __init__(self):
        """Plugin init method."""

        self.description = "System mkinitcpio configuration"

        Plugin.__init__(self)

    def config_system(self, ili_state: IliState):
        """Configure system mkinitcpio."""

        sysmkinitcpio = SysMkinitcpio()

        ili_state.output_callback('Configuring mkinitcpio')

        kwargs = {}
        # If mdadm is in base_packages, we need to enable it in mkinitcpio
        if 'mdadm' in ili_state.base_packages:
            kwargs['mdadm'] = True
        # If lvm2 is in base_packages, we need to enable lvm in mkinitcpio
        if 'lvm2' in ili_state.base_packages:
            kwargs['lvm'] = True

        sysmkinitcpio.configure(ili_state.target_root, **kwargs)

        if ili_state.kernel_preset is None:
            raise RuntimeError('The kernel_preset should of been set')

        ili_state.output_callback('Creating mkinitcpio')
        sysmkinitcpio.create(ili_state.target_root, ili_state.kernel_preset, ili_state.output_callback)
