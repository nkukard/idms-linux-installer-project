# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""AS-IS disk usage strategies."""

from typing import Callable, Dict, List, Optional

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugin import Plugin
from idmslinux_installer.util.sfdisk import Sfdisk


# Ignore warning that we have not overridden all base class methods.
# pylama:ignore=W:select=W023
class DiskUsageASIS(Plugin):
    """AS-IS disk usage strategy class."""

    def __init__(self):
        """Plugin init method."""

        self.description = "AS-IS Disk Usage Strategy"
        Plugin.__init__(self)

    def get_diskusage_strategies(self, ili_state: IliState):
        """Check which strategies we can provided based on the disks list."""

        # If we have more than 1 disk, then AS-IS will work
        if len(ili_state.install_disks) == 1:
            ili_state.add_diskusage_strategy('AS-IS', 'Use disk as-is')

    def commit_diskusage_strategy(self, ili_state: IliState):
        """Commit disk usage strategy and return the root, boot and efi device."""

        # Check the strategy to use for these disks
        if ili_state.diskusage_strategy == 'AS-IS':
            ili_state.output_callback('Partitioning disk')

            # The disk to use for AS-IS is the first disk in the list
            disk = ili_state.install_disks[0]

            # Create an Sfdisk object
            sfdisk = Sfdisk()
            # Add disk partitions
            sfdisk.create_partition(-1, 1, 'BIOS boot')
            sfdisk.create_partition(-1, 500, 'EFI System')
            sfdisk.create_partition(-1, 500, 'Linux filesystem')
            sfdisk.create_partition(-1, -1, 'Linux filesystem')
            sfdisk.write_partitions(disk, output_callback=ili_state.output_callback)

            # Add the block devices to the installer
            ili_state.output_callback('Adding block devices')
            ili_state.add_blockdevice('efi', f'{disk}2')
            ili_state.add_blockdevice('boot', f'{disk}3')
            ili_state.add_blockdevice('root', f'{disk}4')

            # Add the MBR device to the installer
            ili_state.output_callback('Adding device for MBR')
            ili_state.add_boot_mbr(disk)
