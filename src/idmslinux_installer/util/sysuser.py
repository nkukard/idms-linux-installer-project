# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Support class for users on the target system."""

import os
from typing import List

from .asyncsubprocess import AsyncSubprocess


class SysUser:
    """The SysUser class handles configuring users on the target system."""

    def __init__(self):
        """Initialize our class."""

    def add(self, system_path: str, username: str):
        """Add a user to the target system."""

        # Run useradd
        proc = AsyncSubprocess(['arch-chroot', system_path, 'useradd', '--create-home', username])
        output = proc.run()

        # Raise an exception if we didn't get a positive response back
        if proc.retcode != 0:
            raise OSError(f'Failed to run useradd for "{username}", return code {proc.retcode}: {output}')

    def chpasswd(self, system_path: str, username: str, password: str):
        """Change password of a user on the target system."""

        # Setup stdin for the chpasswd command
        input_line = f'{username}:{password}\n'

        # Run chpasswd
        proc = AsyncSubprocess(['arch-chroot', system_path, 'chpasswd', username], input_lines=[input_line])
        output = proc.run()

        # Raise an exception if we didn't get a positive response back
        if proc.retcode != 0:
            raise OSError(f'Failed to run chpasswd for "{username}", return code {proc.retcode}: {output}')

    def write_sshkeys(self, system_path: str, username: str, sshkeys: List[str]):
        """Write out ssh keys to user."""

        # Generate sshkeys lines
        sshkeys_lines: List[str] = [
            f'{x}\n' for x in sshkeys
        ]

        user_ssh_dir = f'{system_path}/home/{username}/.ssh'

        # Make sure the ssh directory exists
        if not os.path.exists(user_ssh_dir):
            try:
                orig_umask = os.umask(0o077)
                os.makedirs(user_ssh_dir)
            finally:
                os.umask(orig_umask)

        # Open the users authorized_keys file and append
        try:
            orig_umask = os.umask(0o177)
            with open(f'{user_ssh_dir}/authorized_keys', 'a') as keys_file:
                # Loop with lines
                for line in sshkeys_lines:
                    # And write
                    keys_file.write(line)
                keys_file.close()
        finally:
            os.umask(orig_umask)
