# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Tests for util/asyncsubprocess.py."""


from typing import List

from idmslinux_installer.util.asyncsubprocess import AsyncSubprocess


class TestAsyncSubprocess():
    """Test the AsyncSubprocess class."""

    callback_lines: List[str]
    callback_results: List[str]

    def test_asyncsubprocess_nocallback(self):
        """Test AsyncSubprocess with no callbacks."""

        # Lines to pipe to the command
        lines = ['hello\n', 'world\n']

        # Spawn the process and give it the lines to send
        process = AsyncSubprocess(['grep', 'hello'], input_lines=lines)

        # Grab the output
        output = process.run()

        assert len(output) == 1, 'One line should be returned'
        assert output == ['hello\n'], 'When we grep the output we should only get "hello" back'

    def test_asyncsubprocess_withcallback(self):
        """Test AsyncSubprocess with callbacks."""

        self.callback_lines = ['hello\n', 'world\n']
        self.callback_results = []

        # Spawn the process and give it the lines to send
        process = AsyncSubprocess(['grep', 'world'], input_callback=self._get_callback_line,
                                  output_callback=self._save_callback_line)

        # Grab the output
        output = process.run()

        assert not output, 'There should be no output returned if we are using callbacks'
        assert not self.callback_lines, 'There should be no more lines left in callback_lines'
        assert len(self.callback_results) == 1, 'There should be only one line in our callback_results'
        assert self.callback_results == ['world\n'], 'The result returned should be "world"'

    def _get_callback_line(self):
        """Return a line for the subprocess."""

        if self.callback_lines:
            line = self.callback_lines.pop(0)
            return line

        return None

    def _save_callback_line(self, line):
        """Save a line from the subprocess."""
        self.callback_results.append(line)
