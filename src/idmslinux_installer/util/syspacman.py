# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Support class for pacman on the target system."""

from typing import List

from .asyncsubprocess import AsyncSubprocess, OutputCallback


class SysPacman:
    """The SysPacman class handles running pacman on the target system."""

    def __init__(self):
        """Initialize our class."""

    def install(self, system_path: str, packages: List[str], output_callback: OutputCallback = None):
        """Install packages on target system."""

        # Build our command args
        cmd_args = ['arch-chroot', system_path, 'pacman', '-S', '--noprogressbar', '--noconfirm']
        # Add the packages to the commandline
        cmd_args.extend(packages)

        # If we didn't get an output_callback, set it to our own class method
        if not output_callback:
            output_callback = self._default_output_callback

        # Run pacman on target system
        proc = AsyncSubprocess(cmd_args, output_callback=output_callback)
        proc.run()

        # Raise an exception if we didn't get a positive response back
        if proc.retcode != 0:
            raise OSError(f'Failed to run pacman on the target system, return code {proc.retcode}')

    def replace_mirrorlist(self, system_path: str, mirrors: List[str]):
        """Set mirrorlist used on target system."""

        # Generate mirrorlist lines
        mirrorlist_lines: List[str] = ['Server = %s\n' % (x) for x in mirrors]

        # Open the mirrorlist file
        with open(f'{system_path}/etc/pacman.d/mirrorlist', 'w') as mirrorlist_file:
            # Loop with lines
            for line in mirrorlist_lines:
                # And write
                mirrorlist_file.write(line)
            mirrorlist_file.close()

    def _default_output_callback(self, line: str):
        line.rstrip()
        print(f'pacman: {line}')
