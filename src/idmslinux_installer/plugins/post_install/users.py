# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Setup users."""

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugin import Plugin
from idmslinux_installer.util.sysuser import SysUser


# Ignore warning that we have not overridden all base class methods.
# pylama:ignore=W:select=W023
class PostInstallUsers(Plugin):
    """Setup users."""

    def __init__(self):
        """Plugin init method."""

        self.description = "Setup users"

        Plugin.__init__(self)

    def pre_install_base(self, ili_state: IliState):
        """Prepare grub."""

        # If we have an extra user we're going to need sudo too
        if ili_state.user_username:
            ili_state.output_callback('Adding "sudo" to base package list')
            ili_state.add_base_package('sudo')
            # Check if we have a password to set
            if ili_state.user_password:
                # Add sudo user, requiring password
                ili_state.add_sudo_user(ili_state.user_username)
            else:
                # Add sudo user, not requiring password
                ili_state.add_sudo_user(ili_state.user_username, require_password=False)
            # If the user has sshkeys defined, we can safely enable sshd
            if ili_state.user_sshkeys:
                ili_state.add_base_package('openssh')
                ili_state.add_enable_service('sshd')

    def post_install(self, ili_state: IliState):
        """Post install task to setup users."""

        sysuser = SysUser()

        # Check if we're changing the root password
        if ili_state.root_password:
            ili_state.output_callback('Setting root password')
            sysuser.chpasswd(ili_state.target_root, 'root', ili_state.root_password)

        # Check if we have an additional user
        if ili_state.user_username:
            ili_state.output_callback(f'Creating user {ili_state.user_username}')
            sysuser.add(ili_state.target_root, ili_state.user_username)
            # Check if we have a password to set
            if ili_state.user_password:
                ili_state.output_callback(f'Setting user {ili_state.user_username} password')
                sysuser.chpasswd(ili_state.target_root, ili_state.user_username, ili_state.user_password)

            # Check if we have ssh keys to write out
            if ili_state.user_sshkeys:
                ili_state.output_callback(f'Adding user {ili_state.user_username} ssh keys')
                sysuser.write_sshkeys(ili_state.target_root, ili_state.user_username, ili_state.user_sshkeys)
