# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Tests for plugins/disk_layout/lvm.py."""

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugins.disk_layouts.lvm import DiskLayoutLVM


class TestDiskLayoutLVM():
    """Test the DiskLayoutLVM class."""

    obj = DiskLayoutLVM()

    def test_disk_layout_with_strategies_single(self):
        """Test LVM disk layout with various strategies."""

        usage_strategies = ('AS-IS', 'MDRAID1', 'MDRAID5', 'MDRAID10')

        # Loop with usage strategies and test
        for usage_strategy in usage_strategies:

            ili_state = IliState()
            ili_state.add_diskusage_strategy(usage_strategy, 'TESTING')

            self.obj.get_disklayout_strategies(ili_state)

            res = ili_state.supported_disklayout_strategies

            assert len(res) == 1, 'One disk layout strategy should be returned'
            assert res[0]['strategy'] == 'LVM', 'The LVM layout strategy should be returned'
