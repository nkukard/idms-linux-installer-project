# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Configure system hosts file."""

from typing import Callable, Dict, List, Optional

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugin import Plugin
from idmslinux_installer.util.etchosts import EtcHosts


# Ignore warning that we have not overridden all base class methods.
# pylama:ignore=W:select=W023
class ConfigSystemEtcHosts(Plugin):
    """Configure system /etc/hosts."""

    _etc_hosts: EtcHosts

    def __init__(self):
        """Plugin init method."""

        self.description = "System configuration for /etc/hosts"
        self._etc_hosts = EtcHosts()

        Plugin.__init__(self)

    def config_system(self, ili_state: IliState):
        """Configure system /etc/hosts."""

        ili_state.output_callback('Configuring /etc/hosts')

        hostnames = ili_state.hostname

        # If we have multiple hostname components, add the first component to the hostnames list
        hostname_components = hostnames.split('.', maxsplit=1)
        if len(hostname_components) > 1:
            hostnames += f' {hostname_components[0]}'

        etchosts = EtcHosts()
        # Add hosts we'll be writing out
        etchosts.add("127.0.1.1", hostnames)
        # Write out hosts entries
        etchosts.write(ili_state.target_root)
