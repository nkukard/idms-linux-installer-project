# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Tests for plugins/disk_usage/asis.py."""

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugins.disk_usage.asis import DiskUsageASIS


class TestDiskUsageASIS():
    """Test the DiskUsageASIS class."""

    obj = DiskUsageASIS()

    def test_one_disk(self):
        """Test ASIS usage strategy with one disk."""

        ili_state = IliState()

        ili_state.add_install_disks(['/dev/tst.sda'])

        self.obj.get_diskusage_strategies(ili_state)

        res = ili_state.supported_diskusage_strategies

        assert len(res) == 1, 'One disk usage strategy should be returned'
        assert res[0]['strategy'] == 'AS-IS', 'The AS-IS strategy should be returned'

    def test_two_disks(self):
        """Test ASIS usage strategy with two disks."""

        ili_state = IliState()

        ili_state.add_install_disks(['/dev/tst.sda', '/dev/tst.sdb'])

        self.obj.get_diskusage_strategies(ili_state)

        res = ili_state.supported_diskusage_strategies

        assert not res, 'No strategies should be returned for two disks'
