# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Install base operating system files."""

from typing import Callable, Dict, List, Optional

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugin import Plugin
from idmslinux_installer.util.pacman import Pacman
from idmslinux_installer.util.pacstrap import Pacstrap


# Ignore warning that we have not overridden all base class methods.
# pylama:ignore=W:select=W023
class InstallBase(Plugin):
    """Install base operating system files."""

    def __init__(self):
        """Plugin init method."""

        self.description = "Install base operating system files"
        Plugin.__init__(self)

    def get_kernels(self, ili_state: IliState):
        """Determine kernels we support."""

        # Add the kernels we support
        ili_state.add_kernel('LTS', 'Long term support')
        ili_state.add_kernel('STABLE', 'Latest stable')
        ili_state.add_kernel('HARDENED', 'Security focused stable')

    def install_base(self, ili_state: IliState):
        """Install base operating system files."""

        ili_state.output_callback('Installing base packages')

        # Grab base package list from pacman
        pacman = Pacman()
        package_list = pacman.get_group_packages('base')
        # We choose the kernel below...
        package_list.remove('linux')

        # Remove vi and replace with vim
        package_list.remove('vi')
        package_list.append('vim')

        # Remove mdadm, we add it ourselves if we need it
        package_list.remove('mdadm')
        # Remove lvm2, we add it ourselves if we need it
        package_list.remove('lvm2')

        # Check which kernel we're going to use
        if ili_state.kernel == 'LTS':
            ili_state.add_base_package('linux-lts')
            ili_state.kernel_preset = 'linux-lts'
        elif ili_state.kernel == 'STABLE':
            ili_state.add_base_package('linux')
            ili_state.kernel_preset = 'linux'
        elif ili_state.kernel == 'HARDENED':
            ili_state.add_base_package('linux-hardened')
            ili_state.kernel_preset = 'linux-hardened'
        else:
            raise RuntimeError(f'Unknown kernel "{ili_state.kernel}"')

        # Suck in the current package list from the install state
        package_list.extend(ili_state.base_packages)

        # Run pacstrap to bootstrap the base operating system files
        pacstrap = Pacstrap()
        pacstrap.run(ili_state.target_root, package_list, ili_state.output_callback)

        # Set state that we have installed the base operating system files
        ili_state.base_installed = True
