# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Plugin handler for the IDMS Linux Installer."""

import inspect
import os
import pkgutil
from typing import List

from .ilistate import IliState


class Plugin:
    """Base plugin class, used as the parent for all plugins we define."""

    description: str
    order: int

    def __init__(self):
        """Plugin __init__ method."""

    def get_diskusage_strategies(self, ili_state: IliState):
        """Populate state with a list of disk usage strategies we can provide based on the list of block devices."""
        raise NotImplementedError

    def get_disklayout_strategies(self, ili_state: IliState):
        """Populate state with a list of disk layout strategies we can provide based on the disk usage."""
        raise NotImplementedError

    def get_networknaming_strategies(self, ili_state: IliState):
        """Populate state with a list of network naming strategies we can provide based on the disk usage."""
        raise NotImplementedError

    def get_kernels(self, ili_state: IliState):
        """Populate state with a list of kernels we can provide."""
        raise NotImplementedError

    def commit_diskusage_strategy(self, ili_state: IliState):
        """Commit the disk usage strategy."""
        raise NotImplementedError

    def commit_disklayout_strategy(self, ili_state: IliState):
        """Commit the disk layout strategy."""
        raise NotImplementedError

    def process_network_interface_mapping(self, ili_state: IliState):
        """Process network interface mapping."""
        raise NotImplementedError

    def create_filesystems(self, ili_state: IliState):
        """Create filesystems."""
        raise NotImplementedError

    def mount_filesystems(self, ili_state: IliState):
        """Mount filesystems."""
        raise NotImplementedError

    def pre_install_base(self, ili_state: IliState):
        """Run before we install the base operating system files."""
        raise NotImplementedError

    def install_base(self, ili_state: IliState):
        """Install base operating system files."""
        raise NotImplementedError

    def config_system(self, ili_state: IliState):
        """Configure system."""
        raise NotImplementedError

    def install_packages(self, ili_state: IliState):
        """Install additional operating system packages."""
        raise NotImplementedError

    def post_install(self, ili_state: IliState):
        """Post install tasks."""
        raise NotImplementedError


class PluginCollection:
    """Plugin loader and collection."""

    # The package name we will be loading plugins from
    plugin_package: str

    # List of plugins we've loaded
    plugins: List = []

    # List of paths we've seen during processing
    seen_paths: List = []

    def __init__(self, plugin_package: str):
        """Set the package we're going to be loading plugins from and initiate the load."""
        self.plugin_package = plugin_package
        self._load_plugins()

    def get_diskusage_strategies(self, ili_state: IliState):
        """Run disk usage strategy plugins so they can add their strategies to the install state."""

        self._call_plugins(ili_state, 'get_diskusage_strategies')

    def get_disklayout_strategies(self, ili_state: IliState):
        """Run disk layout strategy plugins so they can add their strategies to the install state."""

        self._call_plugins(ili_state, 'get_disklayout_strategies')

    def get_networknaming_strategies(self, ili_state: IliState):
        """Run network naming strategy plugins so they can add their strategies to the install state."""

        self._call_plugins(ili_state, 'get_networknaming_strategies')

    def get_kernels(self, ili_state: IliState):
        """Run kernel plugins so they can add their kernels to the install state."""

        self._call_plugins(ili_state, 'get_kernels')

    def process_network_interface_mapping(self, ili_state: IliState):
        """Process network interface mapping."""

        self._call_plugins(ili_state, 'process_network_interface_mapping')

    def commit_diskusage_strategy(self, ili_state: IliState):
        """Call plugins to commit the disk usage strategy."""

        self._call_plugins(ili_state, 'commit_diskusage_strategy')

        # Make sure we've got block devices in our state now
        if not ili_state.blockdevices:
            raise RuntimeError('At least one of the disk usage strategies should of setup the block devices for install.')

    def commit_disklayout_strategy(self, ili_state: IliState):
        """Call plugins to commit the disk layout strategy."""

        self._call_plugins(ili_state, 'commit_disklayout_strategy')

    def create_filesystems(self, ili_state: IliState):
        """Call plugins to create our filesystems."""

        self._call_plugins(ili_state, 'create_filesystems')

        # Make sure we've got filesystems in our state now
        if not ili_state.filesystems:
            raise RuntimeError('At least one of the create_filesystems plugins should of setup the filesystems for install.')

    def mount_filesystems(self, ili_state: IliState):
        """Call plugins to mount our filesystems."""

        self._call_plugins(ili_state, 'mount_filesystems')

        # Make sure we've got filesystems in our state now
        if not ili_state.target_root:
            raise RuntimeError('We should of gotten "target_root" from one of the plugins when mounting filesystems.')

    def pre_install_base(self, ili_state: IliState):
        """Call plugins to before we run the base operating system file installation."""

        self._call_plugins(ili_state, 'pre_install_base')

    def install_base(self, ili_state: IliState):
        """Call plugins to install our base operating system files."""

        self._call_plugins(ili_state, 'install_base')

        # Make sure we've gotten the base operating system files installed
        if not ili_state.base_installed:
            raise RuntimeError('We should of gotten "base_installed" set to True from one of the plugins.')

    def config_system(self, ili_state: IliState):
        """Call plugins to install our base operating system files."""

        self._call_plugins(ili_state, 'config_system')

    def install_packages(self, ili_state: IliState):
        """Call plugins to install additional packages."""

        self._call_plugins(ili_state, 'install_packages')

    def post_install(self, ili_state: IliState):
        """Call plugins to do post-install tasks."""

        self._call_plugins(ili_state, 'post_install')

    #
    # Internals
    #

    def _load_plugins(self):
        """Load plugins from the plugin_package we were provided."""

        self._find_plugins(self.plugin_package)

    def _call_plugins(self, ili_state: IliState, method_name: str):
        # Loop with plugins, if they have overridden the method, then call it
        for plugin in self.plugins:
            if self._plugin_has(plugin, method_name):
                method = getattr(plugin, method_name)
                method(ili_state)

    def _plugin_has(self, obj, method_name):
        """Check if the plugin defines the method."""

        # Grab super class
        super_cls = super(obj.__class__, obj.__class__)
        return getattr(obj.__class__, method_name) != getattr(super_cls, method_name)

    def _find_plugins(self, package):
        """Recursively search the plugin_package and retrieve all plugins."""

        imported_package = __import__(package, fromlist=['VERSION'])

        # Iterate through the modules
        for _, plugin_name, ispkg in pkgutil.iter_modules(imported_package.__path__,
                                                          imported_package.__name__ + '.'):
            # If this is not a package, continue and check the members
            if not ispkg:
                plugin_module = __import__(plugin_name, fromlist=['VERSION'])

                # Grab object members
                object_members = inspect.getmembers(plugin_module, inspect.isclass)

                # Loop with class names
                for (_, class_name) in object_members:
                    # Only add classes that are a sub class of Plugin, but not Plugin itself
                    if issubclass(class_name, Plugin) & (class_name is not Plugin):
                        self.plugins.append(class_name())

        # Look for modules in sub packages
        all_current_paths = []

        if isinstance(imported_package.__path__, str):
            all_current_paths.append(imported_package.__path__)
        else:
            all_current_paths.extend([x for x in imported_package.__path__])

        # Loop with package path
        for pkg_path in all_current_paths:

            # Make sure its not see in our main seen_paths
            if pkg_path not in self.seen_paths:
                # If not add it so we don't process it again
                self.seen_paths.append(pkg_path)
                # Grab all the sub directories of the current package path directory
                sub_dirs = [p for p in os.listdir(pkg_path)
                            if os.path.isdir(os.path.join(pkg_path, p))]

                # Find packages in sub directory
                for sub_dir in sub_dirs:
                    self._find_plugins(package + '.' + sub_dir)
