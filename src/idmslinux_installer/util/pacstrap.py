# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Support class for pacstrap."""

from typing import List, Optional

from .asyncsubprocess import AsyncSubprocess, OutputCallback


class Pacstrap:
    """The Pacstrap class handles our interface to pacstrap."""

    def __init__(self):
        """Initialize our class."""

    def run(self, path: str, packages: Optional[List[str]] = None, output_callback: OutputCallback = None):
        """Run pacstrap."""

        # Build our command args
        cmd_args = ['pacstrap']
        cmd_args.append(path)
        # If we have an additional package list, add it to the command line
        if packages:
            cmd_args.extend(packages)

        # If we didn't get an output_callback, set it to our own class method
        if not output_callback:
            output_callback = self._default_output_callback

        # Run pacstrap
        proc = AsyncSubprocess(cmd_args, output_callback=output_callback)
        proc.run()  # We don't care about the result as we're using an output callback

        # Raise an exception if we didn't get a positive response back
        if proc.retcode != 0:
            raise OSError(f'Failed to run pacstrap, return code {proc.retcode}')

    def _default_output_callback(self, line: str):
        line.rstrip()
        print(f'pacstrap: {line}')
