# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Configure systemd-resolved."""

import os
from typing import Callable, Dict, List, Optional

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugin import Plugin


# Ignore warning that we have not overridden all base class methods.
# pylama:ignore=W:select=W023
class ConfigSystemSystemdResolved(Plugin):
    """Configure systemd-resolved."""

    def __init__(self):
        """Plugin init method."""

        self.description = "Configure systemd-resolved"

        Plugin.__init__(self)

    def config_system(self, ili_state: IliState):
        """Configure systemd-resolved."""

        ili_state.output_callback('Configuring systemd-resolved')

        # If the resolv.conf file exists, delete it
        sys_resolv_conf = f'{ili_state.target_root}/etc/resolv.conf'
        if os.path.exists(sys_resolv_conf):
            os.unlink(sys_resolv_conf)

        # Link systemd-resolved to resolv.conf
        os.symlink('../run/systemd/resolve/stub-resolv.conf', sys_resolv_conf)

        ili_state.output_callback('Adding systemd-resolved to service enable list')
        ili_state.add_enable_service('systemd-resolved')
