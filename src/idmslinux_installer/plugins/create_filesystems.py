# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Create filesystems on the provided block devices."""

from typing import Callable, Dict, List, Optional

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugin import Plugin
from idmslinux_installer.util.mkfs import Mkfs


# Ignore warning that we have not overridden all base class methods.
# pylama:ignore=W:select=W023
class CreateFilesystems(Plugin):
    """Create filesystems on the provided block devices."""

    def __init__(self):
        """Plugin init method."""

        self.description = "Create Filesystems"

        Plugin.__init__(self)

    def create_filesystems(self, ili_state: IliState):
        """Create the filesystems we need on the block devices."""

        # Mount the root filesystem
        ili_state.output_callback('Mounting root filesystem')

        # Create an mkfs object
        mkfs = Mkfs()

        # Check which block devices we need to create filesystems for
        if 'efi' in ili_state.blockdevices:
            ili_state.output_callback('Configuring EFI filesystem')
            uuid = mkfs.run('vfat', ili_state.blockdevices['efi'], fslabel="efi", args=['-F', '32'],
                            output_callback=ili_state.output_callback)
            ili_state.add_filesystem('efi', uuid, 'vfat', ili_state.blockdevices['efi'])

        if 'boot' in ili_state.blockdevices:
            ili_state.output_callback('Configuring boot filesystem')
            uuid = mkfs.run('ext3', ili_state.blockdevices['boot'], fslabel="boot",
                            output_callback=ili_state.output_callback)
            ili_state.add_filesystem('boot', uuid, 'ext3', ili_state.blockdevices['boot'])

        if 'root' in ili_state.blockdevices:
            ili_state.output_callback('Configuring root filesystem')
            uuid = mkfs.run('ext4', ili_state.blockdevices['root'], fslabel="root",
                            output_callback=ili_state.output_callback)
            ili_state.add_filesystem('root', uuid, 'ext4', ili_state.blockdevices['root'])
