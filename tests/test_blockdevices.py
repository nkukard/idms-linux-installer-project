# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Tests for util/blockdevices.py."""


from idmslinux_installer.util.blockdevices import BlockDevices  # type: ignore


class TestBlockDevices():
    """Test the BlockDevices class."""

    blockdevices = BlockDevices(load=False)

    def test_json_decode(self):
        """Test json decoding."""

        self.blockdevices.decode_json('''{
           "blockdevices": [
                 {"path":"/dev/sda", "type":"disk", "size":"8G"},
                 {"path":"/dev/sdb", "type":"disk", "size":"1G"}
           ]
        }''')

        assert len(self.blockdevices.block_devices) == 2, 'Two devices should of been returned'

    def test_str_return(self):
        """Test str return."""

        res = self.blockdevices.block_devices[0].str()

        assert res == '{path: /dev/sda, device_type: disk, size: 8388608, size_str: 8G}', \
            'Test for return value of str(blockdevices)'
