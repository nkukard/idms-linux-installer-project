# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Mount filesystems to obtain a target root."""

from typing import Callable, Dict, List, Optional

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugin import Plugin
from idmslinux_installer.util.mount import Mount


# Ignore warning that we have not overridden all base class methods.
# pylama:ignore=W:select=W023
class MountFilesystems(Plugin):
    """Mount filesystems and set target root."""

    # Directories for /boot and /boot/efi
    _boot_dir: str
    _efi_dir: str

    def __init__(self):
        """Plugin init method."""

        self.description = "Mount Filesystems"

        self._boot_dir = '/boot'
        self._efi_dir = '/boot/efi'

        Plugin.__init__(self)

    def mount_filesystems(self, ili_state: IliState):
        """Loop with filesystems and mount them, setting target root."""

        # Mount the root filesystem
        ili_state.output_callback('Mounting root filesystem')
        filesystem = ili_state.filesystems['root']
        mount = Mount(uuid=filesystem['uuid'], fstype=filesystem['fstype'], mount_point=ili_state.target_mount)
        ili_state.add_mount(mount)
        # Add fstab entry
        ili_state.add_fstab('root', mount.mount_device, '/', filesystem['fstype'])

        # If we have a boot filesystem, use it
        if 'boot' in ili_state.filesystems:
            ili_state.output_callback('Mounting boot filesystem')
            # Determine the boot mount point
            mount_point = '%s%s' % (ili_state.target_mount, self._boot_dir)
            # Mount the boot filesystem
            filesystem = ili_state.filesystems['boot']
            mount = Mount(uuid=filesystem['uuid'], fstype=filesystem['fstype'], mount_point=mount_point)
            ili_state.add_mount(mount)
            # Add fstab entry
            ili_state.add_fstab('boot', mount.mount_device, self._boot_dir, filesystem['fstype'], fspass=2)

        # If we have a efi filesystem, use it
        if 'efi' in ili_state.filesystems:
            ili_state.output_callback('Mounting EFI filesystem')
            # Determine the boot mount point
            mount_point = '%s%s' % (ili_state.target_mount, self._efi_dir)
            # Determine the efi path
            filesystem = ili_state.filesystems['efi']
            # Mount the efi filesystem
            mount = Mount(uuid=filesystem['uuid'], fstype=filesystem['fstype'], mount_point=mount_point)
            ili_state.add_mount(mount)
            # Add fstab entry
            ili_state.add_fstab('efi', mount.mount_device, self._efi_dir, filesystem['fstype'], fspass=2)

        # Set the target root
        ili_state.target_root = ili_state.target_mount
