# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Entry point into the IDMS Linux installer from the commandline."""

import argparse

from idmslinux_installer.ili import Ili, __version__


def main():
    """Entry point for execution from the commandline."""

    print(f'IDMS Linux Installer v{__version__} - Copyright © 2019, AllWorldIT.\n')

    # Start argument parser
    argparser = argparse.ArgumentParser(add_help=False)
    # Create argument group for installer options
    installer_group = argparser.add_argument_group('Installer options')
    installer_group.add_argument('--preseed-uri', dest='preseed_uri', action='store',
                                 help='Load installer preseed config from URI')
    installer_group.add_argument('--textmode', dest='textmode', action='store_true',
                                 help='Run installer in text mode')
    # Create argument group for optionals
    optional_group = argparser.add_argument_group('Optional arguments')
    optional_group.add_argument('-h', '--help', action="help", help="Show this help message and exit")

    # Parse args
    args = argparser.parse_args()

    # Args to pass to installer
    kwargs = {}

    if args.preseed_uri:
        kwargs['preseed_uri'] = args.preseed_uri
    if args.textmode:
        kwargs['textmode'] = args.textmode

    # Fire up the installer
    installer = Ili()
    installer.start(**kwargs)


if __name__ == '__main__':
    main()
