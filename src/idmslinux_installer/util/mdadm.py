# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Support class for mdadm."""

from typing import List

from idmslinux_installer.util.asyncsubprocess import (AsyncSubprocess,
                                                      OutputCallback)


class Mdadm:
    """The Mdadm class handles our interface to mdadm."""

    def __init__(self, load: bool = True):
        """Initialize our class and load system partition types."""

    def create(self, md_device: str, level: int, devices: List[str], output_callback: OutputCallback = None):
        """Create RAID device."""

        # If we didn't get an output_callback, set it to our own class method
        if not output_callback:
            output_callback = self._default_output_callback

        # Work out how many devices we have
        num_devices = len(devices)

        # Run mdadm
        proc = AsyncSubprocess(['mdadm', '--create', md_device, '--level', str(level), '--raid-devices', str(num_devices),
                                '--metadata', 'default', *devices], output_callback=output_callback)
        proc.run()

        # Raise an exception if we didn't get a positive response back
        if proc.retcode != 0:
            raise OSError(f'Failed to create md device {md_device} return code {proc.retcode}')

    def _default_output_callback(self, line: str):
        line.rstrip()
        print(f'mdadm: {line}')
