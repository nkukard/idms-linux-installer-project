# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Support class for sfdisk."""

import re
import subprocess
from typing import Dict, List

from idmslinux_installer.util.asyncsubprocess import (AsyncSubprocess,
                                                      OutputCallback)


class Sfdisk:
    """The Sfdisk class handles our interface to sfdisk."""

    # Partition types sfdisk supports, indexed by name
    partition_types: Dict[str, str] = {}

    # This is the list of partitions we've created below
    partitions: List[Dict]

    def __init__(self, load: bool = True):
        """Initialize our class and load system partition types."""

        # Start with no partitions
        self.partitions = []

        # Check if we should load from initialization
        if (load is True) & (not self.partition_types):
            self.load_partition_types()

    def load_partition_types(self):
        """Load the various partition types supported by sfdisk."""

        # Open subprocess to sfdisk
        sfdisk_output = subprocess.check_output([
            'sfdisk',
            '--label', 'gpt',
            '--list-types',
        ])

        # Split the output up into lines
        lines = sfdisk_output.splitlines()

        # Parse the partition types
        for line in lines:
            # Split off the two parts
            parts = re.split(r'\s+', line.decode(), 1)
            # Exclude title line and the blank line
            if parts[0] in ('Id', ''):
                continue
            self.partition_types[parts[1]] = parts[0]

    def create_partition(self, start: int, size: int, part_type: str):
        """Create a partition."""

        # Check that the partition type exists
        if part_type not in self.partition_types:
            raise ValueError(f'Partition type "{part_type}" does not exist')

        self.partitions.append({
            'start': start,
            'size': size,
            'type': part_type,
        })

    def write_partitions(self, device: str, output_callback: OutputCallback = None):
        """Write out partitions to a disk device."""

        # If we didn't get an output_callback, set it to our own class method
        if not output_callback:
            output_callback = self._default_output_callback

        # Generate partition lines
        partition_lines: List[str] = [
            '%s,%s,%s\n' % (_sanitize_parm(x['start']), _sanitize_parm(x['size']), self.partition_types[x['type']])
            for x in self.partitions
        ]

        # Run sfdisk
        proc = AsyncSubprocess(['sfdisk', '--wipe', 'always', '--wipe-partitions', 'always', '--label', 'gpt', device],
                               input_lines=partition_lines, output_callback=output_callback)
        proc.run()

        # Raise an exception if we didn't get a positive response back
        if proc.retcode != 0:
            raise OSError(f'Failed to write partitions to device {device} return code {proc.retcode}')

    def _default_output_callback(self, line: str):
        line.rstrip()
        print(f'grub: {line}')


def _sanitize_parm(pos: int) -> str:
    """Sanitize sfdisk start/size parameter."""

    if pos == -1:
        return ''

    return '%iM' % pos
