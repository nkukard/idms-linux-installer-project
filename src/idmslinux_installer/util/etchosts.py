# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Support class for /etc/hosts."""

from typing import Dict, List


class EtcHosts:
    """The EtcHosts class handles writing out /etc/hosts files."""

    # This is the list of entries we've created below
    _entries: List[Dict[str, str]]

    def __init__(self):
        """Initialize our class."""

        # Start with no entries
        self._entries = []

    def add(self, ipaddr: str, hostnames: str):
        """Create a hosts entry."""

        # Add entry
        self._entries.append({
            'ipaddr': ipaddr,
            'hostnames': hostnames,
        })

    def write(self, system_path: str):
        """Write out hosts entries."""

        # Generate hosts lines
        hosts_lines: List[str] = [
            '%s\t\t%s\n' % (x['ipaddr'], x['hostnames']) for x in self._entries
        ]

        # Open the hosts file and append
        with open(f'{system_path}/etc/hosts', 'a') as hosts_file:
            # Loop with lines
            for line in hosts_lines:
                # And write
                hosts_file.write(line)
            hosts_file.close()

    @property
    def entries(self):
        """Return the entries."""
        return self._entries
