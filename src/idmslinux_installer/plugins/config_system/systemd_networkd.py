# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Configure systemd networkd."""

import ipaddress
from typing import Callable, Dict, List, Optional

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugin import Plugin
from idmslinux_installer.util.networkdevices import NetworkDevices


# Ignore warning that we have not overridden all base class methods.
# pylama:ignore=W:select=W023
class ConfigSystemdNetworkd(Plugin):
    """Configure systemd networkd."""

    def __init__(self):
        """Plugin init method."""

        self.description = "System configuration systemd networkd"

        Plugin.__init__(self)

    def get_networknaming_strategies(self, ili_state: IliState):
        """Check which strategies we can provided."""

        # Add the strategies we support
        ili_state.add_networknaming_strategy('NONE', 'Do not rename interfaces')
        ili_state.add_networknaming_strategy('AUTO-SEQ', 'Rename en* devices as e0pN')

    def process_network_interface_mapping(self, ili_state: IliState):
        """Commit the network naming to state."""

        # Grab system network devices object
        networkdevices = NetworkDevices()
        # Grab the ethernet devices subset
        ethernet_devices = networkdevices.ethernet

        # Just return now if the strategy is NONE
        if ili_state.networknaming_strategy == 'NONE':
            # Load the network devices as they are
            for device_name in sorted(ethernet_devices):
                ili_state.add_network_interface_mapping(device_name, ethernet_devices[device_name]['mac'])

        # Check if we have the automatic strategy
        elif ili_state.networknaming_strategy == 'AUTO-SEQ':
            # Loop with each device and name sequentially
            seq = 0
            for device_name in sorted(ethernet_devices):
                ili_state.add_network_interface_mapping(f'e0p{seq}', ethernet_devices[device_name]['mac'])
                seq += 1

    def config_system(self, ili_state: IliState):
        """Configure system systemd networkd."""

        ili_state.output_callback('Configuring systemd-networkd')

        # Make sure systemd-networkd starts on boot
        ili_state.add_enable_service('systemd-networkd')

        # If the strategy is not NONE, then we need to name the interfaces
        if ili_state.networknaming_strategy != 'NONE':
            self._write_link_files(ili_state)

        # Loop with network configuration and output config files
        for device_name, config in ili_state.network_interface_attributes.items():
            self._write_network_file(ili_state, device_name, config)

    def _write_link_files(self, ili_state: IliState):
        """Write out link files."""

        # Loop with interfaces name mapping and output config files
        for device_name, device_mac in ili_state.network_interface_mappings.items():
            # Open the hardware address file
            with open(f'{ili_state.target_root}/etc/systemd/network/10-{device_name}.link', 'w') as link_file:
                link_file.write(f'# Created during install\n')
                link_file.write(f'[Match]\n')
                link_file.write(f'Path=pci-*\n')
                link_file.write(f'MACAddress={device_mac}\n')
                link_file.write('\n')
                link_file.write(f'[Link]\n')
                link_file.write(f'Name={device_name}\n')
                link_file.close()

    def _write_network_file(self, ili_state: IliState, device_name: str, config: Dict[str, str]):
        # Open the hardware address file
        with open(f'{ili_state.target_root}/etc/systemd/network/50-{device_name}.network', 'w') as network_file:
            network_file.write(f'# Created during install\n')
            network_file.write(f'[Match]\n')
            network_file.write(f'Name={device_name}\n')
            network_file.write('\n')

            network_file.write('[Network]\n')

            # Check if this interface uses DHCP
            if 'dhcp' in config:
                network_file.write(f'DHCP=yes\n')
            else:
                network_file.write(f'ConfigureWithoutCarrier=yes\n')
                network_file.write(f'IgnoreCarrierLoss=yes\n')

            # Check if we have IP addresses
            if 'ipv4address' in config:
                network_file.write(f'Address=%s\n' % config['ipv4address'])
            if 'ipv6address' in config:
                network_file.write(f'Address=%s\n' % config['ipv6address'])

            # Check if we have gateways
            if 'ipv4gateway' in config:
                network_file.write(f'Gateway=%s\n' % config['ipv4gateway'])
            if 'ipv6gateway' in config:
                network_file.write(f'Gateway=%s\n' % config['ipv6gateway'])

            # Check if we have DNS servers
            if 'dns1address' in config:
                network_file.write(f'DNS=%s\n' % config['dns1address'])
            if 'dns2address' in config:
                network_file.write(f'DNS=%s\n' % config['dns2address'])

            # If ipv4gateway is not within ipv4address's range, then add a route section
            if self._outside_network4(config, 'ipv4address', 'ipv4gateway'):
                network_file.write('\n[Route]\n')
                network_file.write('Destination=%s\n' % config['ipv4gateway'])

            # If ipv6gateway is not within ipv6address's range, then add a route section
            if self._outside_network6(config, 'ipv6address', 'ipv6gateway'):
                network_file.write('\n[Route]\n')
                network_file.write('Destination=%s\n' % config['ipv6gateway'])

            network_file.write(f'\n')
            # Finally close the file
            network_file.close()

    def _outside_network4(self, config: Dict[str, str], network_name: str, address_name: str):
        """Check if a network does not overlaps an address."""

        if (network_name not in config) or (address_name not in config):
            return False

        # Reduce both to networks
        network = ipaddress.IPv4Network(config[network_name], strict=False)
        address = ipaddress.IPv4Network(config[address_name])

        return not network.overlaps(address)

    def _outside_network6(self, config: Dict[str, str], network_name: str, address_name: str):
        """Check if a network does not overlaps an address."""

        if (network_name not in config) or (address_name not in config):
            return False

        # Reduce both to networks
        network = ipaddress.IPv6Network(config[network_name], strict=False)
        address = ipaddress.IPv6Network(config[address_name])

        return not network.overlaps(address)
