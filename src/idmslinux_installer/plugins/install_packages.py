# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Install additional packages."""

from typing import Callable, Dict, List, Optional

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugin import Plugin
from idmslinux_installer.util.syspacman import SysPacman


# Ignore warning that we have not overridden all base class methods.
# pylama:ignore=W:select=W023
class InstallPackages(Plugin):
    """Install additional system packages."""

    def __init__(self):
        """Plugin init method."""

        self.description = "Install additonal system packages"
        Plugin.__init__(self)

    def install_packages(self, ili_state: IliState):
        """Install additional system packages."""

        # If we have packages to install
        if ili_state.packages:
            ili_state.output_callback('Installing packages')
            # Install them
            syspacman = SysPacman()
            syspacman.install(ili_state.target_root, ili_state.packages, ili_state.output_callback)
