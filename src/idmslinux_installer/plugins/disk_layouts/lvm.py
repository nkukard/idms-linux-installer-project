# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""LVM disk layout strategy."""

from typing import List

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugin import Plugin
from idmslinux_installer.util.lvm import Lvm


# Ignore warning that we have not overridden all base class methods.
# pylama:ignore=W:select=W023
class DiskLayoutLVM(Plugin):
    """LVM disk layout strategy class."""

    def __init__(self):
        """Plugin init method."""

        self.description = "LVM Disk Layout Strategy"
        Plugin.__init__(self)

    def get_disklayout_strategies(self, ili_state: IliState):
        """Check which strategies we can provided based on the disk usage."""

        ili_state.add_disklayout_strategy('LVM', 'Use LVM volumes')

    def commit_disklayout_strategy(self, ili_state: IliState):
        """Commit disk layout strategy and add the root, boot and efi filesystem path."""

        # Check the strategy to use for these block devices
        if ili_state.disklayout_strategy == 'LVM':
            ili_state.output_callback('Configuring LVM')

            # Create an lvm object
            lvm = Lvm()

            # What we do here is we take the 'root' device we already have and create a PV/VG/LV
            pv_device = ili_state.blockdevices['root']

            # Create the PV
            lvm.create_pv(pv_device, output_callback=ili_state.output_callback)

            # If we have been setup with RAID, use the name of 'lvm-raid' else use 'main'
            if ili_state.diskusage_strategy.find('RAID') > 0:
                vg_name = 'lvm-raid'
            else:
                vg_name = 'main'

            # Create the VG
            lvm.create_vg(pv_device, vg_name, output_callback=ili_state.output_callback)

            # Create a 10G LV
            lvm.create_lv(vg_name, 'root', f'{ili_state.root_size}G', output_callback=ili_state.output_callback)

            # Replace the 'root' block device
            ili_state.replace_blockdevice('root', f'/dev/{vg_name}/root')

            # Add lvm2 to the base packages
            ili_state.add_base_package('lvm2')
