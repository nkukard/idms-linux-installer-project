# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""System block devices support class."""

import json
import re
import subprocess
from typing import Dict, List


class BlockDevice:
    """Representation of a system block device and associated handling methods."""

    # Path to the system block device
    path: str
    # Type of block device
    device_type: str
    # Size of block device
    size: int  # in KiB
    size_str: str  # string representation

    def __init__(self, device_info: Dict[str, str]):
        """Initialize the BlockDevice object."""

        # Setup the block device attributes
        self.path = device_info['path']
        self.device_type = device_info['type']
        # And parse the size
        self.size_str = device_info['size']
        self.size = parse_size(device_info['size'])

    def str(self):
        """Format this class into a string."""

        # Return string representation of the object
        return ('{path: %s, device_type: %s, size: %s, size_str: %s}' % (
            self.path, self.device_type, self.size, self.size_str
        ))


class BlockDevices:
    """System block device handling class."""

    # Block devices
    block_devices: List[BlockDevice]

    def __init__(self, load: bool = True):
        """Initialize the block devices we get from the system."""

        # Start with a blank list of block devices
        self.block_devices = []

        # Check if we should load from initialization
        if load is True:
            self.load()

    def load(self):
        """Load block devices from lsblk."""

        # Open subprocess to lsblk
        lsblk_output = subprocess.check_output([
            'lsblk',
            '--list', '--noheadings',
            '--output', 'PATH,TYPE,SIZE',
            '--sort', 'NAME',
            '--nodeps',
            '--include', '8,259',
            '--json',
        ])

        # Decode the output we got
        self.decode_json(lsblk_output)

    def decode_json(self, json_str: str):
        """Parse the json we got from lsblk into our device list and create block devices from it."""

        # Grab lsblk devices from the JSON we got
        json_decoded = json.loads(json_str)

        # Create the devices from the structure we got
        self._create_devices(json_decoded['blockdevices'])

    def _create_devices(self, device_list: List):
        """Take a list of devices in a Dict and create the associated BlockDevice objects."""

        # Loop with each device in the list and create a block device
        for device_info in device_list:
            block_device = BlockDevice(device_info)
            # Add block device to our internal list
            self.block_devices.append(block_device)

    def __iter__(self):
        """Return the internal list of block devices as the iterable."""

        return iter(self.block_devices)


#
# Internal functions
#


def parse_size(size_str: str) -> int:
    """Return the size of the disk in KiB from the string size provided in the format of nM, nG, nT."""

    # Parse the size components
    components = re.match(r'(?P<num>[0-9\.]+)(?P<multiplier>[MGT])',
                          size_str)
    # If we didn't get a match, we need to raise an exception
    if components is None:
        raise ValueError('Failed to parse block device size: %s' %
                         size_str)

    # Grab the size
    size = int(components['num'])

    # Check the multiplier we're going to use...
    if components['multiplier'] == 'M':
        size *= 1024
    elif components['multiplier'] == 'G':
        size *= 1024 * 1024
    elif components['multipler'] == 'T':
        size *= 1024 * 1024 * 1024
    else:
        raise ValueError('Failed to parse the "multiplier" portion of \
                the block device size: %s' % components['multiplier'])

    # Set the size
    return size
