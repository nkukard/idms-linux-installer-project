# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Support class for mkinitcpio on the target system."""

import os
import re

from .asyncsubprocess import AsyncSubprocess, OutputCallback


class SysMkinitcpio:
    """The SysMkinitcpio class handles creating an mkinitcpio on the target system."""

    def __init__(self):
        """Initialize our class."""

    def configure(self, system_path: str, **kwargs):
        """Configure mkinitcpio."""

        sys_mkinitcpio_conf = f'{system_path}/etc/mkinitcpio.conf'
        sys_mkinitcpio_conf_new = f'{sys_mkinitcpio_conf}.new'

        # Open the config files and process
        with open(sys_mkinitcpio_conf, 'r') as conf_file, open(sys_mkinitcpio_conf_new, 'w') as conf_file_new:
            # Loop with lines
            for line in conf_file:
                # Check if we can pull out the hooks
                matches = re.match(r'^\s*HOOKS=\((?P<hooks>.*)\)', line)
                if matches:
                    # If we can split it into a list
                    hooks = matches.group('hooks').split()

                    # Replace udev with systemd
                    udev_index = hooks.index('udev')
                    if udev_index:
                        hooks[udev_index] = 'systemd'

                    # Check if we need to add mdadm support
                    if kwargs.get('mdadm'):
                        # Insert mdadm_udev after it
                        hooks.insert(hooks.index('block')+1, 'mdadm_udev')

                    # Check if we need to add lvm support
                    if kwargs.get('lvm'):
                        # Insert sd-lvm2 infront of it
                        hooks.insert(hooks.index('filesystems'), 'sd-lvm2')

                    # Rebuild the line
                    line = 'HOOKS=(' + ' '.join(hooks) + ')\n'

                # Write out line
                conf_file_new.write(line)

            # Close files
            conf_file.close()
            conf_file_new.close()

            # Move new file ontop of old one
            os.replace(sys_mkinitcpio_conf_new, sys_mkinitcpio_conf)

    def create(self, system_path: str, preset: str, output_callback: OutputCallback = None):
        """Create mkinitcpio on target system."""

        # If we didn't get an output_callback, set it to our own class method
        if not output_callback:
            output_callback = self._default_output_callback

        # Run mkinitcpio
        proc = AsyncSubprocess(['arch-chroot', system_path, 'mkinitcpio', '-p', preset], output_callback=output_callback)
        proc.run()

        # Raise an exception if we didn't get a positive response back
        if proc.retcode != 0:
            raise OSError(f'Failed to run mkinitcpio on the target system, return code {proc.retcode}')

    def _default_output_callback(self, line: str):
        line.rstrip()
        print(f'mkinitcpio: {line}')
