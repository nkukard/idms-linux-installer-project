# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Tests for disk usage strategy plugins."""

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugin import PluginCollection


class TestDiskUsagePlugins():
    """Test the disk usage strategy plugins."""

    obj = PluginCollection('idmslinux_installer.plugins')

    def test_one_disk(self):
        """Test disk usage strategy with one disk."""

        ili_state = IliState()

        ili_state.add_install_disks(['/dev/tst.sda'])

        self.obj.get_diskusage_strategies(ili_state)

        res = ili_state.supported_diskusage_strategies

        assert len(res) == 1, 'One disk usage strategy should be returned'
        assert res[0]['strategy'] == 'AS-IS', 'The AS-IS strategy should be returned'

    def test_two_disks(self):
        """Test disk usage strategy with two disks."""

        ili_state = IliState()

        ili_state.add_install_disks(['/dev/tst.sda', '/dev/tst.sdb'])

        self.obj.get_diskusage_strategies(ili_state)

        res = ili_state.supported_diskusage_strategies

        assert len(res) == 1, 'One disk usage strategy should be returned'
        assert res[0]['strategy'] == 'MDRAID1', 'The MDRAID1 strategy should be returned'
