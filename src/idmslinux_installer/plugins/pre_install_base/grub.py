# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Prepare grub."""

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugin import Plugin


# Ignore warning that we have not overridden all base class methods.
# pylama:ignore=W:select=W023
class PreInstallBaseGrub(Plugin):
    """Prepare grub."""

    def __init__(self):
        """Plugin init method."""

        self.description = "Prepare grub"

        Plugin.__init__(self)

    def pre_install_base(self, ili_state: IliState):
        """Prepare grub."""

        if ili_state.filesystems['boot']:
            ili_state.output_callback('Adding "grub" to base package list')
            ili_state.add_base_package('grub')

        if ili_state.filesystems['efi']:
            ili_state.output_callback('Adding "efibootmgr" to base package list')
            ili_state.add_base_package('efibootmgr')
