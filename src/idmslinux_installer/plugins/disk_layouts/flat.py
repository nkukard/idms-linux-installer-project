# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""FLAT disk layout strategy."""

from typing import Callable, Dict, List, Optional

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugin import Plugin
from idmslinux_installer.util.mkfs import Mkfs


# Ignore warning that we have not overridden all base class methods.
# pylama:ignore=W:select=W023
class DiskLayoutFLAT(Plugin):
    """FLAT disk layout strategy class."""

    def __init__(self):
        """Plugin init method."""

        self.description = "FLAT Disk Layout Strategy"
        Plugin.__init__(self)

    def get_disklayout_strategies(self, ili_state: IliState):
        """Check which strategies we can provided based on the disk usage."""

        ili_state.add_disklayout_strategy('FLAT', 'No volume management')

    def commit_disklayout_strategy(self, ili_state: IliState):
        """Commit disk layout strategy and add the root, boot and efi filesystem path."""

        # Check the strategy to use for these block devices
        if ili_state.disklayout_strategy == 'FLAT':
            ili_state.output_callback('Creating FLAT disk layout')

            # We don't actually have to do anything here as we already have the block devices
