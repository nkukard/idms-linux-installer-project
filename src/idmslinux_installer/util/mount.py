# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Support class for mount."""

import os
from typing import List, Optional

from .asyncsubprocess import AsyncSubprocess


class Mount:
    """The Mount class handles our interface to mount."""

    _mount_device: str
    _mount_point: Optional[str] = None

    def __init__(self, fstype: str, mount_point: str, device: Optional[str] = None,
                 uuid: Optional[str] = None):
        """Initialize our class and mount the filesystem."""

        # Check combinations of arguments
        if (not device) and (not uuid):
            raise RuntimeError('Either "device" or "uuid" must be specified')
        if device and uuid:
            raise RuntimeError('Arguments "device" and "uuid" cannot be used at the same time')

        # Work out device part of the mount
        if device:
            self._mount_device = device
        if uuid:
            self._mount_device = f'UUID={uuid}'

        # Build list of args
        cmd_args: List = ['mount', '-t', fstype]
        # Add mount device
        cmd_args.append(self._mount_device)
        # Lastly append the mount point
        cmd_args.append(mount_point)

        # Make sure mount point exists
        if not os.path.exists(mount_point):
            os.makedirs(mount_point)

        # Run mount
        proc = AsyncSubprocess(cmd_args)
        output = proc.run()

        # Raise an exception if we didn't get a positive response back
        if proc.retcode != 0:
            raise OSError(f'Failed to run mount, return code {proc.retcode}: {output}')

        self._mount_point = mount_point

    def umount(self):
        """Unmount the filesystem."""

        if not self._mount_point:
            raise RuntimeError("Trying to umount when not mounted.")

        # Run umount
        proc = AsyncSubprocess(['umount', self._mount_point])
        output = proc.run()

        # Raise an exception if we didn't get a positive response back
        if proc.retcode != 0:
            raise OSError(f'Failed to run umount, return code {proc.retcode}: {output}')

        self._mount_point = None

    @property
    def mount_device(self):
        """Return the mount device."""
        return self._mount_device

    @property
    def mount_point(self):
        """Return the mount point."""
        return self._mount_point
