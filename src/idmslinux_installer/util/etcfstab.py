# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Support class for /etc/fstab."""

from typing import Dict, List


class EtcFstab:
    """The EtcFstab class handles writing out /etc/fstab files."""

    # This is the list of entries we've added below
    entries: List[Dict[str, str]]

    def __init__(self):
        """Initialize our class."""

        # Start with no entries
        self.entries = []

    def add(self, device: str, mount_point: str, fstype: str, **kwargs):
        """Create a fstab entry."""

        # Grab additional items for the fstab
        fsoptions = kwargs.get('options', 'defaults')
        fsdump = kwargs.get('fsdump', 0)
        fspass = kwargs.get('fspass', 1)

        # Add entry
        self.entries.append({
            'device': device,
            'mount_point': mount_point,
            'fstype': fstype,
            'fsoptions': fsoptions,
            'fsdump': fsdump,
            'fspass': fspass,
        })

    def write(self, system_path: str):
        """Write out fstab entries."""

        # Generate fstab lines
        fstab_lines: List[str] = [
            '%s\t\t%s\t\t%s\t%s\t%s %s\n' % (
                x['device'], x['mount_point'], x['fstype'], x['fsoptions'], x['fsdump'], x['fspass']
            )
            for x in self.entries
        ]

        # Open the fstab and append
        with open(f'{system_path}/etc/fstab', 'a') as fstab_file:
            # Loop with lines
            for line in fstab_lines:
                # And write
                fstab_file.write(line)
            fstab_file.close()
