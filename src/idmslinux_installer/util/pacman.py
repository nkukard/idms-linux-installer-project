# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Support class for pacman."""

from typing import List

from .asyncsubprocess import AsyncSubprocess


class Pacman:
    """The Pacman class handles running pacman."""

    # This property tells us if the database was updated
    _database_updated: bool

    def __init__(self):
        """Initialize our class."""

        self._database_updated = False

    def update_database(self):
        """Update pacman database."""

        # Run pacman to update its database
        proc = AsyncSubprocess(['pacman', '-Sy'])
        output = proc.run()

        # Raise an exception if we didn't get a positive response back
        if proc.retcode != 0:
            raise OSError(f'Failed to run pacman, return code {proc.retcode}: {output}')

    def get_group_packages(self, group: str) -> List[str]:
        """Get packages from a group."""

        # Make sure our database was updated
        if not self._database_updated:
            self.update_database()

        # Run pacman
        proc = AsyncSubprocess(['pacman', '-Sg', group])
        output = proc.run()

        # Raise an exception if we didn't get a positive response back
        if proc.retcode != 0:
            raise OSError(f'Failed to run pacman, return code {proc.retcode}: {output}')

        # Return package list, which is the second item in the split
        return [x.split()[1] for x in output]
