# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""IDMS Linux Installer state."""

import atexit
import re
from typing import Any, Dict, List, Optional

from idmslinux_installer.util.asyncsubprocess import OutputCallback
from idmslinux_installer.util.mount import Mount


# Ignore warning that we have too many instance attributes
# pylama:ignore=R:select=R0902
class IliState:
    """IliState is used to track the installer state."""

    #
    # CONFIGURABLE
    #

    # Chosen install disks
    _install_disks: List[str]
    # Chosen strategies
    _diskusage_strategy: Optional[str]
    _disklayout_strategy: Optional[str]
    _networknaming_strategy: Optional[str]
    _kernel: Optional[str]
    # root size in Gbytes
    _root_size: int
    # Hostname
    _hostname: Optional[str]
    # Locale
    _locale: str
    # Timezone
    _timezone: str
    # Mirrorlist
    _mirrorlist: Optional[str]
    # Install microcode
    _install_microcode: Optional[bool]
    # Root password
    _root_password: Optional[str]
    # Extra user username
    _user_username: Optional[str]
    # Extra user password
    _user_password: Optional[str]
    # Extra user ssh keys
    _user_sshkeys: List[str]
    # Services to enable
    _enable_services: List[str]
    # Network
    _network_interface_mappings: Dict[str, str]
    _network_interface_attributes: Dict[str, Dict[str, str]]

    #
    # INTERNALS
    #

    # Supported strategies
    _supported_diskusage_strategies: List[Dict[str, str]]
    _supported_disklayout_strategies: List[Dict[str, str]]
    _supported_networknaming_strategies: List[Dict[str, str]]
    _supported_kernels: List[Dict[str, str]]

    # Block devices we'll be creating fileystems on, indexed by 'efi', 'boot', 'root'
    _blockdevices: Dict[str, str]

    # MBR's which we'll be installing boot records on
    _boot_mbrs: List[str]

    # Filesystems to install on, indexed by 'efi', 'boot', 'root'
    _filesystems: Dict[str, Dict[str, str]]

    # Filesystems we've mounted
    _mounts: List[Mount]

    # Packages we need to install
    _packages: List[str]

    # Target root, this is the directory we're going to be bootstrapping
    _target_root: Optional[str]
    # Read-only mount point for the target root
    _target_mount: str

    # Our fstab that will be written to the target system
    _fstab: Dict[str, Dict[str, str]]

    # Sudo users
    _sudo_users: List[Dict[str, Any]]

    # Packages to install along with the system base
    _base_packages: List[str]

    # Preset to use for mkinitcpio
    _mkinitcpio_preset: Optional[str]

    # This property indicates that the base operating system files were installed
    _base_installed: bool

    # Output callback
    _output_callback: Optional[OutputCallback]

    def __init__(self, output_callback: Optional[OutputCallback] = None):
        """Initialize the object."""

        # These are our configurable properties
        self._install_disks = []
        self._diskusage_strategy = None
        self._disklayout_strategy = None
        self._networknaming_strategy = None
        self._kernel = None
        self._root_size = 10
        self._hostname = None
        self._locale = 'en_US.UTF-8'
        self._timezone = 'UTC'
        self._mirrorlist = None
        self._packages = []
        self._install_microcode = None
        self._root_password = None
        self._user_username = None
        self._user_password = None
        self._user_sshkeys = []
        self._enable_services = []
        self._network_interface_mappings = {}
        self._network_interface_attributes = {}

        # These properties are set during the install process
        self._supported_diskusage_strategies = []
        self._supported_disklayout_strategies = []
        self._supported_networknaming_strategies = []
        self._supported_kernels = []

        self._blockdevices = {}

        self._boot_mbrs = []

        self._filesystems = {}

        self._mounts = []

        self._target_root = None
        self._target_mount = '/installmnt'

        self._fstab = {}

        self._sudo_users = []

        self._base_packages = []

        self._mkinitcpio_preset = None

        self._base_installed = False

        # Check if we have an output_callback, if not set to the dummy one we have
        if output_callback:
            self._output_callback = output_callback
        else:
            self._output_callback = _default_output_callback

        # Register our cleanup operation
        atexit.register(self._cleanup_and_exit)

    # Install disks
    def add_install_disks(self, disks: List[str]):
        """Add install disks."""

        self._install_disks.extend(disks)

    @property
    def install_disks(self):
        """Return the install_disks property."""
        return self._install_disks

    # Disk usage
    def add_diskusage_strategy(self, strategy: str, description: str):
        """Add a disk usage strategy to the list of strategies supported."""

        self._supported_diskusage_strategies.append({'strategy': strategy, 'description': description})

    @property
    def supported_diskusage_strategies(self):
        """Return the diskusage_strategy property."""
        return self._supported_diskusage_strategies

    @property
    def diskusage_strategy(self):
        """Return the diskusage_strategy property."""
        return self._diskusage_strategy

    @diskusage_strategy.setter
    def diskusage_strategy(self, value: str):
        """Set the diskusage_strategy property."""
        self._diskusage_strategy = value

    # Disk layout
    def add_disklayout_strategy(self, strategy: str, description: str):
        """Add a disk layout strategy to the list of strategies supported."""

        self._supported_disklayout_strategies.append({'strategy': strategy, 'description': description})

    @property
    def supported_disklayout_strategies(self):
        """Return the disklayout_strategies property."""
        return self._supported_disklayout_strategies

    @property
    def disklayout_strategy(self):
        """Return the disklayout_strategy property."""
        return self._disklayout_strategy

    @disklayout_strategy.setter
    def disklayout_strategy(self, value: str):
        """Set the disklayout_strategy property."""
        self._disklayout_strategy = value

    # Network naming
    def add_networknaming_strategy(self, strategy: str, description: str):
        """Add a network naming strategy to the list of strategies supported."""

        self._supported_networknaming_strategies.append({'strategy': strategy, 'description': description})

    @property
    def supported_networknaming_strategies(self):
        """Return the networknaming_strategies property."""
        return self._supported_networknaming_strategies

    @property
    def networknaming_strategy(self):
        """Return the networknaming_strategy property."""
        return self._networknaming_strategy

    @networknaming_strategy.setter
    def networknaming_strategy(self, value: str):
        """Set the networknaming_strategy property."""
        self._networknaming_strategy = value

    # Kernels
    def add_kernel(self, kernel: str, description: str):
        """Add a kernel to the list of kernels supported."""

        self._supported_kernels.append({'kernel': kernel, 'description': description})

    @property
    def supported_kernels(self):
        """Return the supported kernels property."""
        return self._supported_kernels

    @property
    def kernel(self):
        """Return the kernel property."""
        return self._kernel

    @kernel.setter
    def kernel(self, value: str):
        """Set the kernel property."""
        self._kernel = value

    @property
    def mkinitcpio_preset(self):
        """Return the mkinitcpio_preset property."""
        return self._kernel

    @mkinitcpio_preset.setter
    def mkinitcpio_preset(self, value: str):
        """Set the mkinitcpio_preset property."""
        self._kernel = value

    # Root size
    @property
    def root_size(self):
        """Return the root_size property."""
        return self._root_size

    @root_size.setter
    def root_size(self, value: int):
        """Set the root_size property."""
        self._root_size = value

    # Hostname
    @property
    def hostname(self):
        """Return the hostname property."""
        return self._hostname

    @hostname.setter
    def hostname(self, value: str):
        """Set the hostname property."""
        self._hostname = value

    # Locale
    @property
    def locale(self):
        """Return the locale property."""
        return self._locale

    @locale.setter
    def locale(self, value: str):
        """Set the locale property."""
        self._locale = value

    # Timezone
    @property
    def timezone(self):
        """Return the timezone property."""
        return self._timezone

    @timezone.setter
    def timezone(self, value: str):
        """Set the timezone property."""
        self._timezone = value

    # Mirrorlist
    @property
    def mirrorlist(self):
        """Return the mirrorlist property."""
        return self._mirrorlist

    @mirrorlist.setter
    def mirrorlist(self, value: str):
        """Set the mirrorlist property."""
        self._mirrorlist = value

    # Packages
    def add_package(self, package: str):
        """Add a package to install."""

        self._packages.append(package)

    def add_base_package(self, package: str):
        """Add a package to install."""

        self._base_packages.append(package)

    @property
    def packages(self):
        """Return the packages property."""
        return self._packages

    @packages.setter
    def packages(self, value: List[str]):
        """Set the packages property."""
        self._packages = value

    @property
    def base_packages(self):
        """Return the base packages property."""
        return self._base_packages

    # Install microcode
    @property
    def install_microcode(self):
        """Return if we should install microcode or not."""
        return self._install_microcode

    @install_microcode.setter
    def install_microcode(self, value: bool):
        """Set the install_microcode property."""
        self._install_microcode = value

    # Users
    @property
    def root_password(self):
        """Return the root_password property."""
        return self._root_password

    @root_password.setter
    def root_password(self, value: str):
        """Set the root_password property."""
        self._root_password = value

    @property
    def user_username(self):
        """Return the user_username property."""
        return self._user_username

    @user_username.setter
    def user_username(self, value: str):
        """Set the user_username property."""
        self._user_username = value

    @property
    def user_password(self):
        """Return the user_password property."""
        return self._user_password

    @user_password.setter
    def user_password(self, value: str):
        """Set the user_password property."""
        self._user_password = value

    def add_user_sshkeys(self, sshkey: str):
        """Add an sshkey to the extra user."""

        self._user_sshkeys.append(sshkey)

    @property
    def user_sshkeys(self):
        """Return the user_sshkeys property."""
        return self._user_sshkeys

    @user_sshkeys.setter
    def user_sshkeys(self, value: List[str]):
        """Set the user_sshkeys property."""
        self._user_sshkeys = value

    # Enable services
    def add_enable_service(self, service: str):
        """Add a service to enable."""

        self._enable_services.append(service)

    @property
    def enable_services(self):
        """Return the enable_services property."""
        return self._enable_services

    # Network
    def add_network_interface_mapping(self, interface: str, mac: str):
        """Add a network interface mapping."""

        self._network_interface_mappings[interface] = mac

    def add_network_interface_attribute(self, interface: str, item: str, value: str):
        """Add a network interface attribute."""

        # If this interface doesn't yet exist, add it
        if interface not in self._network_interface_attributes:
            self._network_interface_attributes[interface] = {}

        self._network_interface_attributes[interface][item] = value

    @property
    def network_interface_mappings(self):
        """Return network_interface_mappings."""
        return self._network_interface_mappings

    @network_interface_mappings.setter
    def network_interface_mappings(self, value: Dict[str, str]):
        """Set the network_interface_mappings property."""
        self._network_interface_mappings = value

    @property
    def network_interface_attributes(self):
        """Return network_interface_attributes."""
        return self._network_interface_attributes

    # Block devices
    def add_blockdevice(self, usage: str, blockdevice: str):
        """Add a block device that we will be creating a filesystems on."""

        if usage not in ['efi', 'boot', 'root']:
            raise RuntimeError(f'Unknown blockdevice usage "{usage}"')

        if usage in self._blockdevices:
            raise RuntimeError(f'Blockdevice usage "{usage}" already set')

        self._blockdevices[usage] = blockdevice

    def replace_blockdevice(self, usage: str, blockdevice: str):
        """Replace a block device."""

        if usage not in ['efi', 'boot', 'root']:
            raise RuntimeError(f'Unknown blockdevice usage "{usage}"')

        if usage not in self._blockdevices:
            raise RuntimeError(f'Blockdevice usage "{usage}" not set')

        self._blockdevices[usage] = blockdevice

    @property
    def blockdevices(self):
        """Return the block devices we should create filesystems on."""
        return self._blockdevices

    # Boot MBR handling
    def add_boot_mbr(self, blockdevice: str):
        """Add a block device to install an MBR on."""

        self._boot_mbrs.append(blockdevice)

    @property
    def boot_mbrs(self):
        """Return the list of block devices that we should install MBR's on."""
        return self._boot_mbrs

    # Filesystems
    def add_filesystem(self, usage: str, uuid: str, fstype: str, device: str):
        """Add a filesystem to install on."""

        if usage not in ['efi', 'boot', 'root']:
            raise RuntimeError(f'Unknown filesystem usage "{usage}"')

        if usage in self._filesystems:
            raise RuntimeError(f'Filesystem usage "{usage}" already set')

        self._filesystems[usage] = {'uuid': uuid, 'fstype': fstype, 'device': device}

    @property
    def filesystems(self):
        """Return the filesystems we should install on."""
        return self._filesystems

    # Mounts
    def add_mount(self, mount: Mount):
        """Add a mount to our mount list."""

        self._mounts.append(mount)

    # fstab entries
    def add_fstab(self, usage: str, device: str, mount_point: str, fstype: str, **kwargs):
        """Add a mount to our mount list."""

        if usage not in ['efi', 'boot', 'root']:
            raise RuntimeError(f'Unknown fstab usage "{usage}"')

        if usage in self._fstab:
            raise RuntimeError(f'The fstab usage "{usage}" is already set')

        # Add fstab entry
        self._fstab[usage] = {
            'device': device,
            'mount_point': mount_point,
            'fstype': fstype,
        }
        # Loop through optional items
        for option in ['options', 'fsdump', 'fspass']:
            if kwargs.get(option):
                self._fstab[usage][option] = str(kwargs.get(option))

    @property
    def fstab(self):
        """Return the fstab object we'll write to the target system."""
        return self._fstab

    # sudo entries
    def add_sudo_user(self, username: str, require_password: bool = True):
        """Add a sudo user."""

        # Add sudo entry
        self._sudo_users.append({
            'username': username,
            'require_password': require_password,
        })

    @property
    def sudo_users(self):
        """Return the sudo_users list we'll write to the target system."""
        return self._sudo_users

    # Target Root
    @property
    def target_root(self):
        """Return the target root we're going to bootstrap."""
        return self._target_root

    @target_root.setter
    def target_root(self, value: str):
        """Set the target_root property."""
        self._target_root = value

    @property
    def target_mount(self):
        """Return the target mount path we're going to use to mount the target_root."""
        return self._target_mount

    # Base installed
    @property
    def base_installed(self):
        """Return if the base operating systems were installed."""
        return self._base_installed

    @base_installed.setter
    def base_installed(self, value: bool):
        """Set if the base operating was installed."""
        self._base_installed = value

    # Output callback
    @property
    def output_callback(self):
        """Return the output_callback property."""
        return self._output_callback

    @output_callback.setter
    def output_callback(self, value: OutputCallback):
        """Set the output_callback property."""
        self._output_callback = value

    # Cleanup function
    def _cleanup_and_exit(self):
        """Cleanup and exit."""

        # Loop with any mounts we have and unmount them
        for mount in reversed(self._mounts):
            # Unmount and remove
            mount.umount()
            self._mounts.remove(mount)

    def __str__(self):
        """Return string representation of this object."""
        string = ''

        for attr in dir(self):
            string += "state.%s = %r\n" % (attr, getattr(self, attr))

        return string


def _default_output_callback(line: str, percent: Optional[int] = None):
    status_line = re.sub('\\s*$', '', line)
    # Ignore blank lines
    if not status_line:
        return
    print(f'IliState({percent}): {status_line}')
