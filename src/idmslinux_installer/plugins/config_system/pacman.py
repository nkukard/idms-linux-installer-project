# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Configure system pacman."""

from typing import Callable, Dict, List, Optional

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugin import Plugin
from idmslinux_installer.util.syspacman import SysPacman


# Ignore warning that we have not overridden all base class methods.
# pylama:ignore=W:select=W023
class ConfigSystemPacman(Plugin):
    """Configure system pacman."""

    def __init__(self):
        """Plugin init method."""

        self.description = "System configuration for pacman"

        Plugin.__init__(self)

    def config_system(self, ili_state: IliState):
        """Configure system pacman."""

        if ili_state.mirrorlist:
            ili_state.output_callback('Writing pacman mirrorlist')
            syspacman = SysPacman()
            # Write out mirrorlist file
            syspacman.replace_mirrorlist(ili_state.target_root, ili_state.mirrorlist)
