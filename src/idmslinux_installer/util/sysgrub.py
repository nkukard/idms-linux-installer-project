# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Support class for grub on the target system."""

import os
import re
from typing import Optional

from .asyncsubprocess import AsyncSubprocess, OutputCallback


class SysGrub:
    """The SysGrub class handles configuring and installing grub on the target system."""

    def __init__(self):
        """Initialize our class."""

    def configure(self, system_path: str, output_callback: Optional[OutputCallback] = None, **kwargs):
        """Configure grub."""

        sys_grub_conf = '/boot/grub/grub.cfg'

        sys_default_grub = f'{system_path}/etc/default/grub'
        sys_default_grub_new = f'{sys_default_grub}.new'

        # If we didn't get an output_callback, set it to our own class method
        if not output_callback:
            output_callback = self._default_output_callback

        # Open the config files and process
        with open(sys_default_grub, 'r') as conf_file, open(sys_default_grub_new, 'w') as conf_file_new:
            # Loop with lines
            for line in conf_file:
                # Check if we can pull out the modules
                matches = re.match(r'^\s*GRUB_PRELOAD_MODULES="(?P<modules>.*)"', line)
                if matches:
                    # If we can split it into a list
                    modules = matches.group('modules').split()

                    # Check if we need to add lvm support
                    if kwargs.get('lvm'):
                        # Append lvm to the end of the list
                        modules.append('lvm')

                    # Rebuild the line
                    line = 'GRUB_PRELOAD_MODULES="' + ' '.join(modules) + '"\n'

                # Write out line
                conf_file_new.write(line)

            # Close files
            conf_file.close()
            conf_file_new.close()

            # Move new file ontop of old one
            os.replace(sys_default_grub_new, sys_default_grub)

        # Run grub-mkconfig
        proc = AsyncSubprocess(['arch-chroot', system_path, 'grub-mkconfig', '-o', sys_grub_conf],
                               output_callback=output_callback)
        proc.run()

        # Raise an exception if we didn't get a positive response back
        if proc.retcode != 0:
            raise OSError(f'Failed to run grub-mkconfig on the target system, return code {proc.retcode}')

    def install_mbr(self, system_path: str, device: str, output_callback: OutputCallback = None):
        """Install grub on the MBR."""

        # If we didn't get an output_callback, set it to our own class method
        if not output_callback:
            output_callback = self._default_output_callback

        # Run grub-install
        proc = AsyncSubprocess(['arch-chroot', system_path, 'grub-install', device],
                               output_callback=output_callback)
        proc.run()

        # Raise an exception if we didn't get a positive response back
        if proc.retcode != 0:
            raise OSError(f'Failed to run grub-install on the target device {device}, return code {proc.retcode}')

    def install_efi(self, system_path: str, efi_dir: str, output_callback: OutputCallback = None):
        """Install grub on the EFI."""

        # If we didn't get an output_callback, set it to our own class method
        if not output_callback:
            output_callback = self._default_output_callback

        # Run grub-install
        proc = AsyncSubprocess(['arch-chroot', system_path, 'grub-install', '--target', 'x86_64-efi',
                                '--efi-directory', efi_dir, '--bootloader-id', 'IDMS Linux'],
                               output_callback=output_callback)
        proc.run()

        # Raise an exception if we didn't get a positive response back
        if proc.retcode != 0:
            raise OSError(f'Failed to run grub-install on the efi directory {efi_dir}, return code {proc.retcode}')

    def _default_output_callback(self, line: str):
        line.rstrip()
        print(f'grub: {line}')
