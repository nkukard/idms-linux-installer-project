# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Tests for util/mkfs.py."""


import os
from tempfile import mkstemp
from typing import List

from idmslinux_installer.util.mkfs import Mkfs


class TestMkfs():
    """Test the Mkfs class."""

    output: List[str]

    def test_mkfs(self):
        """Test Mkfs actually writing a partition table to disk."""

        # Create temp file
        (filedes, fname) = mkstemp()
        # Seek 8M in and write 1
        os.pwrite(filedes, b'1', 8000000)
        os.close(filedes)

        obj = Mkfs()
        res = obj.run('ext3', fname, fslabel="test")

        os.unlink(fname)

        assert res is not None, "The return of mkfs.run must not be None"

    def test_mkfs_with_callback(self):
        """Test Mkfs actually writing a partition table to disk with a callback."""

        self.output = []

        # Create temp file
        (filedes, fname) = mkstemp()
        # Seek 8M in and write 1
        os.pwrite(filedes, b'1', 8000000)
        os.close(filedes)

        obj = Mkfs()
        res = obj.run('ext3', fname, fslabel="test", output_callback=self._output_callback)

        os.unlink(fname)

        assert res is not None, "The return of mkfs.run must not be None"
        assert self.output, "We should have output stored by the callback"

    def _output_callback(self, line: str):
        """Output callback to test we got something."""

        self.output.append(line)
