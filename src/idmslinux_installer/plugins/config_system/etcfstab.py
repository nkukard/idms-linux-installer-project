# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Configure system fstab file."""

from typing import Callable, Dict, List, Optional

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugin import Plugin
from idmslinux_installer.util.etcfstab import EtcFstab


# Ignore warning that we have not overridden all base class methods.
# pylama:ignore=W:select=W023
class ConfigSystemEtcFstab(Plugin):
    """Configure system /etc/fstab."""

    _etc_fstab: EtcFstab

    def __init__(self):
        """Plugin init method."""

        self.description = "System configuration for /etc/fstab"
        self._etc_fstab = EtcFstab()

        Plugin.__init__(self)

    def config_system(self, ili_state: IliState):
        """Configure system /etc/fstab."""

        ili_state.output_callback('Configuring /etc/fstab')

        # Loop with each usage and add the associated entry
        for usage in ['root', 'boot', 'efi']:
            self._etc_fstab.add(**ili_state.fstab[usage])

        # Write out entries
        self._etc_fstab.write(ili_state.target_root)
