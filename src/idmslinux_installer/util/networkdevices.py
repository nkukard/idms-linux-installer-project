# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Support class for system network devices."""

import os
import re
from typing import Dict


class NetworkDevices:
    """The NetworkDevices class handles system network devices."""

    # Network device list, this is global
    _network_devices: Dict[str, Dict[str, str]] = {}

    def __init__(self):
        """Initialize our class."""

        # If we've not been initialized yet, initialize
        if not self._network_devices:
            self.refresh()

    def refresh(self):
        """Refresh network devices from system."""

        class_dir = '/sys/class/net'

        self._network_devices = {}

        # List the directory contents so we can get the device names
        network_devices = os.listdir(class_dir)
        for device_name in network_devices:
            # Initialize device dict
            self._network_devices[device_name] = {}
            # Open the hardware address file
            with open(f'{class_dir}/{device_name}/address', 'r') as address_file:
                # Read in MAC and strip the newline
                self._network_devices[device_name]['mac'] = address_file.read().rstrip()
                # Finally close file
                address_file.close()

    @property
    def ethernet(self):
        """Return a dict of only ethernet devices."""

        # Filter out ethernet interfaces
        ethernet_devices = {x: self._network_devices[x] for x in self._network_devices if re.match('^en[pso]', x)}

        return ethernet_devices
