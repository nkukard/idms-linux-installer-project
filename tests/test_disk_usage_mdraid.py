# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Tests for plugins/disk_usage/mdraid.py."""

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugins.disk_usage.mdraid import DiskUsageMDRAID


class TestDiskUsageMDRAID():
    """Test the DiskUsageMDRAID class."""

    obj = DiskUsageMDRAID()

    def test_one_disk(self):
        """Test MDRAID with one disk."""

        ili_state = IliState()

        ili_state.add_install_disks(['/dev/tst.sda'])

        self.obj.get_diskusage_strategies(ili_state)

        res = ili_state.supported_diskusage_strategies

        assert not res, 'No disks usage strategies should be returned for one disk'

    def test_two_disks(self):
        """Test MDRAID with two disks."""

        ili_state = IliState()

        ili_state.add_install_disks(['/dev/tst.sda', '/dev/tst.sdb'])

        self.obj.get_diskusage_strategies(ili_state)

        res = ili_state.supported_diskusage_strategies

        assert len(res) == 1, 'One disk usage strategy should be returned with two disks'

        assert res[0]['strategy'] == 'MDRAID1', 'The MDRAID1 strategy should be returned'

    def test_three_disks(self):
        """Test MDRAID with three disks."""

        ili_state = IliState()

        ili_state.add_install_disks(['/dev/tst.sda', '/dev/tst.sdb', '/dev/tst.sdc'])

        self.obj.get_diskusage_strategies(ili_state)

        res = ili_state.supported_diskusage_strategies

        assert len(res) == 2, 'Two disk usage strategies should be returned with three disks'

        assert res[0]['strategy'] == 'MDRAID1', 'The MDRAID1 strategy should be returned for three disks'
        assert res[1]['strategy'] == 'MDRAID5', 'The MDRAID5 strategy should be returned for three disks'

    def test_four_disks(self):
        """Test MDRAID with four disks."""

        ili_state = IliState()

        ili_state.add_install_disks(['/dev/tst.sda', '/dev/tst.sdb', '/dev/tst.sdc', '/dev/tst.sdd'])

        self.obj.get_diskusage_strategies(ili_state)

        res = ili_state.supported_diskusage_strategies

        assert len(res) == 4, 'Four disk usage strategies should be returned with four disks'

        assert res[0]['strategy'] == 'MDRAID1', 'The MDRAID1 strategy should be returned for four disks'
        assert res[1]['strategy'] == 'MDRAID5', 'The MDRAID5 strategy should be returned for four disks'
        assert res[2]['strategy'] == 'MDRAID6', 'The MDRAID6 strategy should be returned for four disks'
        assert res[3]['strategy'] == 'MDRAID10', 'The MDRAID10 strategy should be returned for four disks'

    def test_five_disks(self):
        """Test MDRAID with five disks."""

        ili_state = IliState()

        ili_state.add_install_disks(['/dev/tst.sda', '/dev/tst.sdb', '/dev/tst.sdc', '/dev/tst.sdd', '/dev/tst.sde'])

        self.obj.get_diskusage_strategies(ili_state)

        res = ili_state.supported_diskusage_strategies

        assert len(res) == 3, 'Three disk usage strategies should be returned with five disks'

        assert res[0]['strategy'] == 'MDRAID1', 'The MDRAID1 strategy should be returned for five disks'
        assert res[1]['strategy'] == 'MDRAID5', 'The MDRAID5 strategy should be returned for five disks'
        assert res[2]['strategy'] == 'MDRAID6', 'The MDRAID6 strategy should be returned for five disks'

    def test_six_disks(self):
        """Test MDRAID with six disks."""

        ili_state = IliState()

        ili_state.add_install_disks(['/dev/tst.sda', '/dev/tst.sdb', '/dev/tst.sdc', '/dev/tst.sdd', '/dev/tst.sde',
                                     '/dev/tst.sdf'])

        self.obj.get_diskusage_strategies(ili_state)

        res = ili_state.supported_diskusage_strategies

        assert len(res) == 4, 'Four disk usage strategies should be returned with six disks'

        assert res[0]['strategy'] == 'MDRAID1', 'The MDRAID1 strategy should be returned for six disks'
        assert res[1]['strategy'] == 'MDRAID5', 'The MDRAID5 strategy should be returned for six disks'
        assert res[2]['strategy'] == 'MDRAID6', 'The MDRAID6 strategy should be returned for six disks'
        assert res[3]['strategy'] == 'MDRAID10', 'The MDRAID10 strategy should be returned for six disks'
