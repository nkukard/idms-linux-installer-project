# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Prepare microcode."""

from typing import Callable, Dict, List, Optional

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugin import Plugin


# Ignore warning that we have not overridden all base class methods.
# pylama:ignore=W:select=W023
class PreInstallBaseMicrocode(Plugin):
    """Prepare system microcode."""

    def __init__(self):
        """Plugin init method."""

        self.description = "Prepare microcode"

        Plugin.__init__(self)

    def pre_install_base(self, ili_state: IliState):
        """Prepare system microcode."""

        # Check if we are installing microcode, if we are add the packages to the list to install
        if ili_state.install_microcode:
            ili_state.output_callback('Adding "intel-ucode" to base package list')
            ili_state.add_base_package('intel-ucode')
            ili_state.output_callback('Adding "amd-ucode" to base package list')
            ili_state.add_base_package('amd-ucode')
