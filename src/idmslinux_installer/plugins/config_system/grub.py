# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Configure system grub."""

from typing import Callable, Dict, List, Optional

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugin import Plugin
from idmslinux_installer.util.sysgrub import SysGrub


# Ignore warning that we have not overridden all base class methods.
# pylama:ignore=W:select=W023
class ConfigSystemGrub(Plugin):
    """Configure system grub."""

    def __init__(self):
        """Plugin init method."""

        self.description = "System grub configuration"

        Plugin.__init__(self)

    def config_system(self, ili_state: IliState):
        """Configure system grub."""

        sysgrub = SysGrub()

        kwargs = {}

        # If lvm2 is in base_packages, we need to enable lvm in grub
        if 'lvm2' in ili_state.base_packages:
            kwargs['lvm'] = True

        # Loop with each disk we must install an MBR on
        for device in ili_state.boot_mbrs:
            ili_state.output_callback(f'Installing grub MBR on {device}')
            sysgrub.install_mbr(ili_state.target_root, device, ili_state.output_callback)

        # Install grub in the efi directory if we have an efi filesystem
        if ili_state.fstab['efi']:
            ili_state.output_callback('Installing grub EFI in %s' % ili_state.fstab['efi']['mount_point'])
            sysgrub.install_efi(ili_state.target_root, ili_state.fstab['efi']['mount_point'], ili_state.output_callback)

        # We configure grub after the installation becaues this is when the /boot/grub dir exists
        sysgrub.configure(ili_state.target_root, ili_state.output_callback)
