# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Support class for /etc/sudoers.d."""

from typing import Any, Dict, List


class EtcSudoers:
    """The EtcSudoers class handles writing out /etc/sudoers.d files."""

    # This is the list of entries we've created below
    _entries: List[Dict[str, Any]]

    def __init__(self):
        """Initialize our class."""

        # Start with no entries
        self._entries = []

    def add_user(self, username: str, require_password: bool = True):
        """Create a sudoers.d entry."""

        # Add entry
        self._entries.append({
            'username': username,
            'require_password': require_password,
        })

    def write(self, system_path: str):
        """Write out sudoers entries."""

        for entry in self._entries:
            username = entry['username']
            # Open the sudoers file and append
            with open(f'{system_path}/etc/sudoers.d/{username}', 'a') as sudoers_file:
                sudoers_file.write('# Added during install\n')
                # Check if we require a password or not
                if entry['require_password']:
                    sudoers_file.write(f'{username} ALL=(ALL) ALL\n')
                else:
                    sudoers_file.write(f'{username} ALL=(ALL) NOPASSWD: ALL\n')
                # Finally close file
                sudoers_file.close()
