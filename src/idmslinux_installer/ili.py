# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Guts of the IDMS Linux Installer."""

import ipaddress
import json
import locale
import re
from configparser import ConfigParser
from typing import Any, Optional

import requests
from dialog import Dialog  # type: ignore

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugin import PluginCollection
from idmslinux_installer.util.blockdevices import BlockDevices

__version__ = "0.0.1"


class Ili:
    """Ili is the IDMS Linux Installer class.

    This class handles the entire install process.
    """

    # The main dialog handle
    _dialog: Dialog
    # Running in text mode
    _textmode: bool
    # Percent done
    _percent_done: int

    # This is our preseed config if we have one
    _preseed: ConfigParser

    # Plugins
    _plugins: PluginCollection

    # Block devices on the system
    _block_devices: BlockDevices

    # This is the state of the installation
    state: IliState

    def __init__(self):
        """Initialize the object."""

        # Fire up Dialog
        self._dialog = Dialog(dialog='dialog', autowidgetsize=True)
        self._dialog.set_background_title(f'IDMS Linux Installer v{__version__}')
        self._textmode = True
        self._percent_done = 0

        # Initialize the config parser to None
        self._preseed = None

        # Load plugins
        self._plugins = PluginCollection('idmslinux_installer.plugins')

        # Initialize the install state
        self.state = IliState()

        # Blank the locale
        locale.setlocale(locale.LC_ALL, '')

        # Set output callback
        self.state.output_callback = self._status_callback

    def start(self, preseed_uri: Optional[str] = None, **kwargs):
        """Start the installer."""

        # Load preseed URI if it was provided
        if preseed_uri:
            self._status_callback(f'Downloading preseed "{preseed_uri}"...')
            self._load_preseed(preseed_uri)
        else:
            # Check if we're operating in text mode
            self._textmode = kwargs.get('textmode', False)

        if not self.state.install_disks:
            # Load the system block devices
            self._block_devices = BlockDevices()
            self._get_install_disks()

        if not self.state.diskusage_strategy:
            self._get_diskusage_strategy()

        if not self.state.disklayout_strategy:
            self._get_disklayout_strategy()

        if not self.state.networknaming_strategy:
            self._get_networknaming_strategy()

        if not self.state.kernel:
            self._get_kernels()

        # Process network interface mapping before we get network config so the devices have the right names
        self._plugins.process_network_interface_mapping(self.state)

        # If we don't have any attributes defined somewhere, we need to get them from the user
        if not self.state.network_interface_attributes:
            # Loop with all devices
            for device_name in self.state.network_interface_mappings:
                self._get_network_config(device_name)

        # Get users choice to install microcode or not
        if self.state.install_microcode is None:
            self._get_install_microcode()

        # Set hostname
        if not self.state.hostname:
            self._get_hostname()

        # Setup root password
        if (not self.state.root_password) and (not preseed_uri):
            self._get_root_password()

        # Setup extra user
        if not self.state.user_username:
            self._get_user_username()
            # If we now have a username, get the user password
            if self.state.user_username:
                self._get_user_password()

        if not self._textmode:
            self._dialog.gauge_start("Preparing...", title="Installing...", width=75, height=7)

        # Loop with install steps
        steps = [
            self._plugins.commit_diskusage_strategy,
            self._plugins.commit_disklayout_strategy,
            self._plugins.create_filesystems,
            self._plugins.mount_filesystems,
            self._plugins.pre_install_base,
            self._plugins.install_base,
            self._plugins.config_system,
            self._plugins.install_packages,
            self._plugins.post_install,
        ]
        for step in steps:
            step(self.state)
            # Update percentage complete
            self._percent_done = int((float(steps.index(step)+1)/float(len(steps)))*100)

        self._percent_done = 100
        self._status_callback("Install Done")

        # End off
        if not self._textmode:
            self._dialog.gauge_stop()

    # C901 - Ignore warning about code complexity
    # R0912 - Ignore warning that we have about the number of branches
    # R0915 - Ignore warning about number of statements
    # pylama:ignore=C901,R0912,R0915
    def _load_preseed(self, preseed_uri: str):
        """Load installer configuration from preseed file."""

        # Parse config
        self._preseed = ConfigParser()

        # HTTP
        if re.match('^https?://', preseed_uri):
            try:
                # Grab HTTP resource
                resource = requests.get(preseed_uri, headers={'User-Agent': f'Ili/{__version__}'})
                config_data = resource.text
            except requests.exceptions.RequestException as exception:
                self._critical_error('Error loading preseed', str(exception))

        # File
        elif re.match('^/', preseed_uri):
            try:
                # Read config file
                with open(preseed_uri, 'r') as config_file:
                    config_data = config_file.read()
            except OSError as exception:
                self._critical_error('Error loading preseed', str(exception))

        else:
            self._critical_error('Error with preseed URI', f'Preseed URI type is not supported:\n{preseed_uri}')

        # Read config as string
        self._preseed.read_string(config_data)

        # Grab the preseed sections
        sections = self._preseed.sections()

        # Process storage section
        if self._preseed.has_section('storage'):
            # Process install disks
            if self._preseed.has_option('storage', 'install_disks'):
                self._status_callback('preseed storage -> install_disks')
                self.state.add_install_disks(self._preseed_read_json('storage', 'install_disks'))
            # Process diskusage strategy
            if self._preseed.has_option('storage', 'diskusage_strategy'):
                self._status_callback('preseed storage -> diskusage_strategy')
                self.state.diskusage_strategy = self._preseed.get('storage', 'diskusage_strategy').upper()
            # Process disklayout strategy
            if self._preseed.has_option('storage', 'disklayout_strategy'):
                self._status_callback('preseed storage -> disklayout_strategy')
                self.state.disklayout_strategy = self._preseed.get('storage', 'disklayout_strategy').upper()
            # Process root_size
            if self._preseed.has_option('system', 'root_size'):
                self._status_callback('preseed system -> root_size')
                self.state.root_size = int(self._preseed.get('system', 'root_size'))

        # Process system section
        if self._preseed.has_section('system'):
            # Process hostname
            if self._preseed.has_option('system', 'hostname'):
                self._status_callback('preseed system -> hostname')
                self.state.hostname = self._preseed.get('system', 'hostname')
            # Process locale
            if self._preseed.has_option('system', 'locale'):
                self._status_callback('preseed system -> locale')
                self.state.hostname = self._preseed.get('system', 'locale')
            # Process timezone
            if self._preseed.has_option('system', 'timezone'):
                self._status_callback('preseed system -> timezone')
                self.state.hostname = self._preseed.get('system', 'timezone')
            # Process install_microcode
            if self._preseed.has_option('system', 'install_microcode'):
                self._status_callback('preseed system -> install_microcode')
                self.state.hostname = self._preseed.getboolean('system', 'install_microcode')
            # Process kernel
            if self._preseed.has_option('system', 'kernel'):
                self._status_callback('preseed system -> kernel')
                self.state.kernel = self._preseed.get('system', 'kernel')

        # Process repository section
        if self._preseed.has_section('repository'):
            # Process mirrorlist
            if self._preseed.has_option('repository', 'mirrorlist'):
                self._status_callback('preseed repository -> mirrorlist')
                self.state.mirrorlist = self._preseed_read_json('repository', 'mirrorlist')
            # Process packages
            if self._preseed.has_option('repository', 'packages'):
                self._status_callback('preseed repository -> packages')
                self.state.mirrorlist = self._preseed_read_json('repository', 'mirrorlist')

        # Process users section
        if self._preseed.has_section('users'):
            # Process root_password
            if self._preseed.has_option('users', 'root_password'):
                self._status_callback('preseed users -> root_password')
                self.state.root_password = self._preseed.get('users', 'root_password')
            # Process user_username
            if self._preseed.has_option('users', 'user_username'):
                self._status_callback('preseed users -> user_username')
                self.state.user_username = self._preseed.get('users', 'user_username')
            # Process user_password
            if self._preseed.has_option('users', 'user_password'):
                self._status_callback('preseed users -> user_password')
                self.state.user_password = self._preseed.get('users', 'user_password')
            # Process user_sshkeys
            if self._preseed.has_option('users', 'user_sshkeys'):
                self._status_callback('preseed users -> user_sshkeys')
                self.state.user_sshkeys = self._preseed_read_json('users', 'user_sshkeys')

        # Process services section
        if self._preseed.has_section('services'):
            # Process enable_services
            if self._preseed.has_option('services', 'enable_services'):
                self._status_callback('preseed services -> enable_services')
                self.state.mirrorlist = json.loads(self._preseed.get('services', 'enable_services'))

        # Process network section
        if self._preseed.has_section('network'):
            # Process networknaming strategy
            if self._preseed.has_option('network', 'networknaming_strategy'):
                self._status_callback('preseed network -> networknaming_strategy')
                self.state.networknaming_strategy = self._preseed.get('network', 'networknaming_strategy').upper()
            # Process interface mapping
            if self._preseed.has_option('network', 'interface_mapping'):
                self._status_callback('preseed network -> interface_mapping')
                self.state.network_interface_mappings = self._preseed_read_json('network', 'interface_mapping')

        # Loop with network device settings
        network_interface_sections = [x for x in sections if x.startswith('network ')]
        for section in network_interface_sections:
            # Grab device name
            device_name = section.replace('network ', '')
            # Loop with section items
            for item, value in self._preseed.items(section):
                self.state.add_network_interface_attribute(device_name, item, value)
    # pylama:select=C901,R0912,R0915

    def _get_install_disks(self):
        """Get the user to choose the disks to install on."""

        disks = []

        # Loop while no disks were selected
        while not disks:
            # Display a checklist with the disks on it
            code, disks = self._dialog.checklist(
                'Select disks to install on',
                height=20, width=40, list_height=10,
                title='Disk Selection',
                choices=[(x.path, x.size_str, False) for x in self._block_devices],
            )

            # If the user selected cancel, we need to abort
            if code == self._dialog.DIALOG_CANCEL:
                exit(0)

            # If no disks were selected, we need to display an error, and do it again
            if not disks:
                self._error('ERROR', 'No disks were selected to install on!')

        # Save the install disks
        self.state.add_install_disks(disks)

    def _get_diskusage_strategy(self):
        """Get the disk usage strategy from the user."""

        # Get available diskusage strategies
        self._plugins.get_diskusage_strategies(self.state)

        chosen_strategy = ''

        # Loop while no disk usage strategy was selected
        while not chosen_strategy:
            # Display a radiolist with the disk usage strategies
            code, chosen_strategy = self._dialog.radiolist(
                'Select disk usage strategy',
                height=20, width=40, list_height=10,
                title='Disk Usage Strategy',
                choices=[
                    (x['strategy'], x['description'], 0) for x in self.state.supported_diskusage_strategies
                ],
            )

            # If the user selected cancel, we need to abort
            if code == self._dialog.DIALOG_CANCEL:
                exit(0)

            # If no disk usage strategy was selected, we need to display an error, and do it again
            if not chosen_strategy:
                self._error('ERROR', 'No disk usage strategy was selected!')

        # Set the diskusage strategy
        self.state.diskusage_strategy = chosen_strategy

    def _get_disklayout_strategy(self):
        """Get disk layout strategy from user."""

        # Get available disklayout strategies
        self._plugins.get_disklayout_strategies(self.state)

        chosen_strategy = ''

        # Loop while no disk layout strategy was selected
        while not chosen_strategy:
            # Display a radiolist with the disk layout strategies
            code, chosen_strategy = self._dialog.radiolist(
                'Select disk layout strategy',
                height=20, width=40, list_height=10,
                title='Disk Layout Strategy',
                choices=[
                    (x['strategy'], x['description'], False) for x in self.state.supported_disklayout_strategies
                ],
            )

            # If the user selected cancel, we need to abort
            if code == self._dialog.DIALOG_CANCEL:
                exit(0)

            # If no disk layout strategy was selected, we need to display an error, and do it again
            if not chosen_strategy:
                self._error('ERROR', 'No disk layout strategy was selected!')

        # Set the disklayout strategy
        self.state.disklayout_strategy = chosen_strategy

    def _get_networknaming_strategy(self):
        """Get network naming strategy from user."""

        # Get available networknaming strategies
        self._plugins.get_networknaming_strategies(self.state)

        chosen_strategy = ''

        # Loop while no network naming strategy was selected
        while not chosen_strategy:
            # Display a radiolist with the network naming strategies
            code, chosen_strategy = self._dialog.radiolist(
                'Select network naming strategy',
                height=20, width=50, list_height=10,
                title='Network Naming Strategy',
                choices=[
                    (x['strategy'], x['description'], False) for x in self.state.supported_networknaming_strategies
                ],
            )

            # If the user selected cancel, we need to abort
            if code == self._dialog.DIALOG_CANCEL:
                exit(0)

            # If no strategy was selected, we need to display an error, and do it again
            if not chosen_strategy:
                self._error('ERROR', 'No network naming strategy was selected!')

        # Set the networknaming strategy
        self.state.networknaming_strategy = chosen_strategy

    def _get_kernels(self):
        """Get kernel to use from user."""

        # Get available kernels
        self._plugins.get_kernels(self.state)

        chosen_kernel = ''

        # Loop while no network naming strategy was selected
        while not chosen_kernel:
            # Display a radiolist with the kernels
            code, chosen_kernel = self._dialog.radiolist(
                'Select kernel to use...',
                height=20, width=40, list_height=10,
                title='Kernel',
                choices=[
                    (x['kernel'], x['description'], False) for x in self.state.supported_kernels
                ],
            )

            # If the user selected cancel, we need to abort
            if code == self._dialog.DIALOG_CANCEL:
                exit(0)

            # If no kernel was selected, we need to display an error, and do it again
            if not chosen_kernel:
                self._error('ERROR', 'No network naming strategy was selected!')

        # Set the kernel to use
        self.state.kernel = chosen_kernel

    def _get_network_config(self, interface: str):
        """Get network device config from user."""

        chosen_addressing = ''

        # Get user to choose the network addressing type
        while not chosen_addressing:
            # Display a radiolist with the network addressing types
            code, chosen_addressing = self._dialog.radiolist(
                f'Select addressing type for {interface}',
                height=20, width=50, list_height=10,
                title='Network Addressing Type',
                choices=[
                    ('NONE', 'No configuration', False),
                    ('STATIC', 'Static addressing', False),
                    ('DHCP', 'Automatic DHCP', False),
                ],
            )

            # If the user selected cancel, we need to abort
            if code == self._dialog.DIALOG_CANCEL:
                exit(0)

            # If no addressing type was selected, we need to display an error, and do it again
            if not chosen_addressing:
                self._error('ERROR', 'No network addressing type was selected!')

        # Return now if we have no addressing type
        if chosen_addressing == 'NONE':
            return

        # If we were set to DHCP, add the attribute and return
        if chosen_addressing == 'DHCP':
            self.state.add_network_interface_attribute(interface, 'dhcp', 'yes')
            return

        # Get user to choose IPv4 address
        while 1:
            # Display an inputbox for the IPv4 address
            code, ipv4address_raw = self._dialog.inputbox(
                f'Enter IPv4 address for {interface} (in CIDR format)...',
                height=8, width=60,
                title='IPv4 Address',
            )

            # If the user selected cancel, we need to abort
            if code == self._dialog.DIALOG_CANCEL:
                exit(0)

            # If no IPv4 address was selected, complain
            if not ipv4address_raw:
                self._error('ERROR', 'No IPv4 address was selected!')
                continue

            try:
                ipaddress.IPv4Network(ipv4address_raw, strict=False)
                self.state.add_network_interface_attribute(interface, 'ipv4address', ipv4address_raw)
                break
            except ipaddress.AddressValueError:
                self._error('ERROR', f'The IPv4 address "{ipv4address_raw}" is not valid!')

        # Get user to choose IPv4 gateway
        while 1:
            # Display an inputbox for the IPv4 gateway
            code, ipv4gateway_raw = self._dialog.inputbox(
                f'Enter IPv4 gateway for {interface} (blank for none)...',
                height=8, width=60,
                title='IPv4 Gateway',
            )

            # If the user selected cancel, we need to abort
            if code == self._dialog.DIALOG_CANCEL:
                exit(0)

            # If no IPv4 gateway was entered break, else process it
            if not ipv4gateway_raw:
                break
            else:
                try:
                    ipaddress.IPv4Address(ipv4gateway_raw)
                    self.state.add_network_interface_attribute(interface, 'ipv4gateway', ipv4gateway_raw)
                    break
                except ipaddress.AddressValueError:
                    self._error('ERROR', f'The IPv4 gateway "{ipv4gateway_raw}" is not valid!')

        # Get user to choose IPv6 address
        has_ipv6 = False
        while 1:
            # Display an inputbox for the IPv6 address
            code, ipv6address_raw = self._dialog.inputbox(
                f'Enter IPv6 address for {interface} (blank for none)...',
                height=8, width=60,
                title='IPv6 Address',
            )

            # If the user selected cancel, we need to abort
            if code == self._dialog.DIALOG_CANCEL:
                exit(0)

            # If no IPv6 address was entered break, else process it
            if not ipv6address_raw:
                break
            else:
                try:
                    ipaddress.IPv6Network(ipv6address_raw, strict=False)
                    self.state.add_network_interface_attribute(interface, 'ipv6address', ipv6address_raw)
                    has_ipv6 = True
                    break
                except ipaddress.AddressValueError:
                    self._error('ERROR', f'The IPv6 address "{ipv6address_raw}" is not valid!')

        # If we have IPv6
        if has_ipv6:
            # Get user to choose IPv6 gateway
            while 1:
                # Display an inputbox for the IPv6 gateway
                code, ipv6gateway_raw = self._dialog.inputbox(
                    f'Enter IPv6 gateway for {interface} (blank for none)...',
                    height=8, width=60,
                    title='IPv6 Gateway',
                )

                # If the user selected cancel, we need to abort
                if code == self._dialog.DIALOG_CANCEL:
                    exit(0)

                # If no IPv6 gateway was entered break, else process it
                if not ipv6gateway_raw:
                    break
                else:
                    try:
                        ipaddress.IPv6Address(ipv6gateway_raw)
                        self.state.add_network_interface_attribute(interface, 'ipv6gateway', ipv6gateway_raw)
                        break
                    except ipaddress.AddressValueError:
                        self._error('ERROR', f'The IPv6 gateway "{ipv6gateway_raw}" is not valid!')

        # Get user to choose DNS1 address
        has_dns1 = False
        while 1:
            # Display an inputbox for the DNS1 address
            code, dns1address_raw = self._dialog.inputbox(
                f'Enter DNS1 address for {interface} (blank for none)...',
                height=8, width=60,
                title='DNS1 Address',
            )

            # If the user selected cancel, we need to abort
            if code == self._dialog.DIALOG_CANCEL:
                exit(0)

            # If no DNS1 address was entered break, else process it
            if not dns1address_raw:
                break
            else:
                try:
                    ipaddress.ip_address(dns1address_raw)
                    self.state.add_network_interface_attribute(interface, 'dns1address', dns1address_raw)
                    has_dns1 = True
                    break
                except ValueError:
                    self._error('ERROR', f'The DNS1 address "{dns1address_raw}" is not valid!')

        # Get user to choose DNS2 address
        if has_dns1:
            while 1:
                # Display an inputbox for the DNS2 address
                code, dns2address_raw = self._dialog.inputbox(
                    f'Enter DNS2 address for {interface} (blank for none)...',
                    height=8, width=60,
                    title='DNS2 Address',
                )

                # If the user selected cancel, we need to abort
                if code == self._dialog.DIALOG_CANCEL:
                    exit(0)

                # If no DNS2 address was entered break, else process it
                if not dns2address_raw:
                    break
                else:
                    try:
                        ipaddress.ip_address(dns2address_raw)
                        self.state.add_network_interface_attribute(interface, 'dns2address', dns2address_raw)
                        break
                    except ValueError:
                        self._error('ERROR', f'The DNS2 address "{dns2address_raw}" is not valid!')

    def _get_install_microcode(self):
        """Get the users choice to install microcode."""

        install_microcode = None

        # Loop while no disk layout strategy was selected
        while install_microcode is None:
            # Display a radiolist with the network addressing types
            code, install_microcode = self._dialog.radiolist(
                f'Install CPU microcode updates?',
                height=20, width=50, list_height=10,
                title='Install Microcode',
                choices=[
                    ('YES', 'Install CPU microcode updates', False),
                    ('NO', 'Do not install CPU microcode', False),
                ],
            )

            # If the user selected cancel, we need to abort
            if code == self._dialog.DIALOG_CANCEL:
                exit(0)

            # If no option was chosen, display a warning
            if not install_microcode:
                self._error('ERROR', 'No option was selected regarding microcode install!')

        # Set the install_microcode attribute
        if install_microcode == 'YES':
            self.state.install_microcode = True
        else:
            self.state.install_microcode = False

    def _get_hostname(self):
        """Get hostname from user."""

        hostname = ''

        # Loop while no disk layout strategy was selected
        while not hostname:
            # Display an inputbox for the hostname
            code, hostname = self._dialog.inputbox(
                'Enter system hostname...',
                height=8, width=40,
                title='Hostname',
            )

            # If the user selected cancel, we need to abort
            if code == self._dialog.DIALOG_CANCEL:
                exit(0)

        # Set the hostname
        self.state.hostname = hostname

    def _get_root_password(self):
        """Get root password from user."""

        root_password = ''

        # Display an inputbox for the root_password
        code, root_password = self._dialog.passwordbox(
            'Enter root password (optional)...',
            height=8, width=40, insecure=True,
            title='Root Password',
        )

        # If the user selected cancel, we need to abort
        if code == self._dialog.DIALOG_CANCEL:
            exit(0)

        # Set the root password if we got one
        if root_password:
            self.state.root_password = root_password

    def _get_user_username(self):
        """Get user username from user."""

        user_username = ''

        # Display an inputbox for the user_username
        code, user_username = self._dialog.inputbox(
            'Enter extra user username (optional)...',
            height=8, width=50,
            title='Extra User Username',
        )

        # If the user selected cancel, we need to abort
        if code == self._dialog.DIALOG_CANCEL:
            exit(0)

        # Set the user username
        if user_username:
            self.state.user_username = user_username

    def _get_user_password(self):
        """Get user password from user."""

        user_password = ''

        # Display an inputbox for the user_password
        code, user_password = self._dialog.passwordbox(
            'Enter user password (optional)...',
            height=8, width=50, insecure=True,
            title='User Password',
        )

        # If the user selected cancel, we need to abort
        if code == self._dialog.DIALOG_CANCEL:
            exit(0)

        # Set the user password if we got one
        if user_password:
            self.state.user_password = user_password

    def _preseed_read_json(self, section: str, option: str) -> Any:
        """Read a ConfigParser option and pass it through json.loads."""
        # Grab value
        value = self._preseed.get(section, option)

        try:
            return json.loads(value)
        except json.decoder.JSONDecodeError as exception:
            self._critical_error('Error parsing preseed', f'Error parsing pressed [{section}][{option}]: {exception}\n{value}')

    def _error(self, title: str, message: str):
        """Display error message to user."""

        self._dialog.msgbox(
            message,
            title=title, width=70, height=7)

    def _critical_error(self, *args):
        """Display error message to user and exit."""

        self._error(*args)
        exit(1)

    def _status_callback(self, line: str):
        """Status callback to give user feedback on the status of installation."""

        # Fix up line
        status_line = re.sub('\\s*$', '', line)
        # If its blank, ignore it
        if not status_line:
            return

        # Check if we're running in text mode
        if not self._textmode:
            if len(status_line) > 70:
                status_line = status_line[0:70] + '...'
            self._dialog.gauge_update(text=status_line[0:70], percent=self._percent_done, update_text=True)
        else:
            print(f'LOG[{self._percent_done}]: {status_line}')
