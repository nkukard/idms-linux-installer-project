# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Support class for hostname."""

from .asyncsubprocess import AsyncSubprocess


class SysHostname:
    """The SysHostname class handles setting the hostname in a system."""

    def __init__(self):
        """Initialize our class."""

    def set(self, system_path: str, hostname: str):
        """Set the system hostname."""

        # Run systemd-firstboot to set the hostname
        proc = AsyncSubprocess(['arch-chroot', system_path, 'systemd-firstboot', '--hostname', hostname])
        output = proc.run()

        # Raise an exception if we didn't get a positive response back
        if proc.retcode != 0:
            raise OSError(f'Failed to run systemd-firstboot to set hostname, return code {proc.retcode}: {output}')
