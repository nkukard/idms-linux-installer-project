# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Support class for lvm."""

from typing import Optional

from idmslinux_installer.util.asyncsubprocess import (AsyncSubprocess,
                                                      OutputCallback)


class Lvm:
    """The Lvm class handles our interface to lvm."""

    def __init__(self, load: bool = True):
        """Initialize our class and load system partition types."""

    def create_pv(self, device: str, output_callback: OutputCallback = None):
        """Create LVM physical volume."""

        # If we didn't get an output_callback, set it to our own class method
        if not output_callback:
            output_callback = self._default_output_callback

        # Run lvm to create the physical volume
        proc = AsyncSubprocess(['pvcreate', device], output_callback=output_callback)
        proc.run()

        # Raise an exception if we didn't get a positive response back
        if proc.retcode != 0:
            raise OSError(f'Failed to create LVM PV on "{device}", return code {proc.retcode}')

    def create_vg(self, pv_device: str, vg_name: str, pe_size: Optional[str] = None, output_callback: OutputCallback = None):
        """Create LVM volume group."""

        # If we didn't get an output_callback, set it to our own class method
        if not output_callback:
            output_callback = self._default_output_callback

        # Use 32M physical extent size by default
        if not pe_size:
            pe_size = '32M'

        # Run lvm to create the physical volume
        proc = AsyncSubprocess(['vgcreate', '--physicalextentsize', pe_size, vg_name, pv_device], output_callback=output_callback)
        proc.run()

        # Raise an exception if we didn't get a positive response back
        if proc.retcode != 0:
            raise OSError(f'Failed to create LVM VG "{vg_name}" on "{pv_device}", return code {proc.retcode}')

    def create_lv(self, vg_name: str, lv_name: str, lv_size: str, output_callback: OutputCallback = None):
        """Create LVM volume."""

        # If we didn't get an output_callback, set it to our own class method
        if not output_callback:
            output_callback = self._default_output_callback

        # Work out the size option we're going to use
        if lv_size.find('%') > 0:
            size_opt = '--extents'
        else:
            size_opt = '--size'

        # Run lvm to create the physical volume
        proc = AsyncSubprocess(['lvcreate', '--wipesignatures', 'y', '--yes', size_opt, lv_size, '--name', lv_name, vg_name],
                               output_callback=output_callback)
        proc.run()

        # Raise an exception if we didn't get a positive response back
        if proc.retcode != 0:
            raise OSError(f'Failed to create LVM LV "{lv_name}" on VG "{vg_name}", return code {proc.retcode}')

    def _default_output_callback(self, line: str):
        line.rstrip()
        print(f'lvm: {line}')
