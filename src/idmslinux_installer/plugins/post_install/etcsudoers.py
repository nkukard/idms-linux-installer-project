# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Configure system sudoers."""

from typing import Callable, Dict, List, Optional

from idmslinux_installer.ilistate import IliState
from idmslinux_installer.plugin import Plugin
from idmslinux_installer.util.etcsudoers import EtcSudoers


# Ignore warning that we have not overridden all base class methods.
# pylama:ignore=W:select=W023
class ConfigSystemEtcSudoers(Plugin):
    """Configure system /etc/sudoers.d."""

    def __init__(self):
        """Plugin init method."""

        self.description = "System configuration for /etc/sudoers.d"

        Plugin.__init__(self)

    def post_install(self, ili_state: IliState):
        """Configure system /etc/sudoers.d."""

        ili_state.output_callback('Configuring /etc/sudoers.d')

        sudoers = EtcSudoers()

        # Loop with user and add to sudoers
        for user in ili_state.sudo_users:
            sudoers.add_user(user['username'], user['require_password'])

        # Write out hosts entries
        sudoers.write(ili_state.target_root)
