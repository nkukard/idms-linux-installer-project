# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Tests for util/sfdisk.py."""


import os
from tempfile import mkstemp

import pytest

from idmslinux_installer.util.sfdisk import Sfdisk


class TestSfdisk():
    """Test the Sfdisk class."""

    obj = Sfdisk()

    def test_sfdisk_partition_types(self):
        """Test Sfdisk partition types."""

        assert self.obj.partition_types['Linux filesystem'] == '0FC63DAF-8483-4772-8E79-3D69D8477DE4', \
            'We should be able to find the partition type "Linux filesystem"'

    def test_sfdisk_create_invalid_partition(self):
        """Test Sfdisk partition partition creation."""

        with pytest.raises(ValueError):
            assert self.obj.create_partition(-1, 100, 'invalid type'), \
                'Using an invalid partition type should raise ValueError'

    def test_sfdisk_create_partition(self):
        """Test Sfdisk partition partition creation."""

        self.obj.create_partition(-1, 100, 'Linux filesystem')

        assert self.obj.partitions[0]['type'] == 'Linux filesystem', 'The partition we just created is not in the object'

    def test_sfdisk_write(self):
        """Test Sfdisk actually writing a partition table to disk."""

        # Create temp file
        (filedes, fname) = mkstemp()
        # Seek 8M in and write 1
        os.pwrite(filedes, b'1', 8000000)
        os.close(filedes)

        # Commit partition table
        self.obj.write_partitions(fname)

        os.unlink(fname)
