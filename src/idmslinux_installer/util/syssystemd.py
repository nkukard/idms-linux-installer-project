# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Support class for systemd on the target system."""

from typing import List

from .asyncsubprocess import AsyncSubprocess, OutputCallback


class SysSystemd:
    """The SysSystemd class handles running systemd tools on the target system."""

    def __init__(self):
        """Initialize our class."""

    def enable_service(self, system_path: str, services: List[str], output_callback: OutputCallback = None):
        """Enable services on the target system."""

        # Build our command args
        cmd_args = ['arch-chroot', system_path, 'systemctl', 'enable']
        # Add the services to the commandline
        cmd_args.extend(services)

        # If we didn't get an output_callback, set it to our own class method
        if not output_callback:
            output_callback = self._default_output_callback

        # Run systemctl on target system
        proc = AsyncSubprocess(cmd_args, output_callback=output_callback)
        proc.run()

        # Raise an exception if we didn't get a positive response back
        if proc.retcode != 0:
            raise OSError(f'Failed to run systemctl on the target system, return code {proc.retcode}')

    def _default_output_callback(self, line: str):
        line.rstrip()
        print(f'systemd: {line}')
