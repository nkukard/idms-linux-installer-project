# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Support class for mkfs."""

from typing import List

from .asyncsubprocess import AsyncSubprocess
from .blkid import Blkid


class Mkfs:
    """The Mkfs class handles our interface to mkfs."""

    def __init__(self):
        """Initialize our class ."""

    def run(self, fstype: str, device: str, **kwargs) -> str:
        """Run mkfs and return the blkid of the filesystem.

        Args accepted are:
            'fslabel' - Optional filesystem label
            'output_callback' - Output callback, called for every line received from mkfs
            'args' - Extra args for mkfs
        """

        # Build list of args
        cmd_args: List = ['mkfs', '--type', fstype]
        if 'fslabel' in kwargs:
            # Check label usage
            if fstype == 'vfat':
                cmd_args.append('-n')
            else:
                cmd_args.append('-L')
            cmd_args.append(kwargs['fslabel'])
        # Perhaps we have extra args specified, if we do, use them
        if 'args' in kwargs:
            cmd_args.extend(kwargs['args'])
        # Lastly append the device we're working on
        cmd_args.append(device)

        output_callback = kwargs.get('output_callback', self._default_output_callback)

        # Run mkfs
        proc = AsyncSubprocess(cmd_args, output_callback=output_callback)
        proc.run()  # We don't care about the result we get, as we using a callback

        # Raise an exception if we didn't get a positive response back
        if proc.retcode != 0:
            raise OSError(f'Failed to run mkfs on device {device} return code {proc.retcode}')

        # Grab device attributes
        blkid = Blkid()
        blkid_attrs = blkid.probe(device)

        # If we don't have the FS UUID, then we need to raise an exception
        if 'ID_FS_UUID' not in blkid_attrs:
            raise OSError(f'Failed to get FS UUID from blkid for device {device}')

        return blkid_attrs['ID_FS_UUID']

    def _default_output_callback(self, line: str):
        line.rstrip()
        print(f'mkfs: {line}')
