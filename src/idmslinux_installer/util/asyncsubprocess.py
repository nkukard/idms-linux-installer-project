# Copyright (c) 2019, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Support class for asyncio subprocesses."""

import asyncio
import subprocess
from typing import Callable, List, Optional

# Define our callback types
InputCallback = Callable[[], str]
OutputCallback = Callable[[str], None]


class AsyncSubprocess:
    """The AsyncSubprocess class handles curating a subprocess under asyncio to send and receive data."""

    # Command and args to create the subprocess
    args: List[str]

    # Subprocess stdin writer
    stdin: asyncio.StreamWriter

    # Callbacks to read and write to
    input_callback: Optional[InputCallback]
    output_callback: Optional[OutputCallback]

    # Lists storing data to send to the subprocess, and data we got from it when callbacks are not defined
    input_lines: Optional[List[str]]
    output_lines: List[str]

    # Return code of the subprocess
    retcode: int

    def __init__(self, args: List[str],
                 input_lines: Optional[List[str]] = None,
                 input_callback: Optional[InputCallback] = None,
                 output_callback: Optional[OutputCallback] = None):
        """Initialize our class ."""

        self.args = args

        self.input_callback = input_callback
        self.output_callback = output_callback

        self.input_lines = input_lines
        # If we don't have an output callback, initialize output_lines
        if not output_callback:
            self.output_lines = []

    def run(self) -> List[str]:
        """Run the subprocess."""

        async_loop = asyncio.get_event_loop()

        # If the event loop is closed
        # if async_loop.is_closed():
        #     Create a new one
        #     async_loop = asyncio.new_event_loop()
        #     asyncio.set_event_loop(async_loop)

        # Run subprocess in event loop
        self.retcode = async_loop.run_until_complete(self._create_subprocess())
        # async_loop.close()

        # If we don't have an output callback, return the output_lines
        if not self.output_callback:
            return self.output_lines

        return []

    async def _create_subprocess(self):
        """Create and run the subprocess."""

        # Create subprocess
        process = await asyncio.create_subprocess_exec(*self.args, stdin=subprocess.PIPE,
                                                       stdout=subprocess.PIPE,
                                                       stderr=subprocess.STDOUT)

        # Set the stdin handle
        self.stdin = process.stdin

        # Create task to write to the process stdin
        asyncio.create_task(self._pump_input())

        # Loop with output and call the output callback, if we don't have one add to the output list
        async for line in process.stdout:
            if self.output_callback:
                self.output_callback(line.decode('utf-8'))
            else:
                self.output_lines.append(line.decode('utf-8'))
        # data = bytearray()
        # while True:
        #    print(f'LOOP')
        #    chunk = await process.stdout.read(1)
        #    print(f'CHUNK: {chunk}')
        #    if not chunk:
        #        print(f"BREAKING")
        #        break
        #    data += chunk

        # Wait for process to exit
        return await process.wait()

    async def _pump_input(self):
        """Send input to the subprocess."""

        try:
            # If we have an input callback
            if self.input_callback:
                # Loop with each line we get back and send it to the subprocess
                while 1:
                    line = self.input_callback()
                    # If we didn't get anything back, break
                    if not line:
                        break
                    # Write line and drain
                    self.stdin.write(bytes(line, 'utf-8'))
                    await self.stdin.drain()
            # If we don't have an input callback
            elif self.input_lines:
                # Suck the lines in from input_lines
                for line in self.input_lines:
                    # Write line and drain
                    self.stdin.write(bytes(line, 'utf-8'))
                    await self.stdin.drain()
        finally:
            self.stdin.close()
